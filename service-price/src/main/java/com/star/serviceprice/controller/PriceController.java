package com.star.serviceprice.controller;

import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.request.ForecastPriceDTO;
import com.star.serviceprice.service.PriceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class PriceController {

    @Autowired
    private PriceService forecastPriceService;

    /**
     * 计算预估价格
     * @param forecastPriceDTO 经纬度
     * @return
     */
    @PostMapping("/forecast-price")
    public ResponseResult forecastPrice(@RequestBody ForecastPriceDTO forecastPriceDTO) {

        String depLongitude = forecastPriceDTO.getDepLongitude();
        String depLatitude = forecastPriceDTO.getDepLatitude();
        String destLongitude = forecastPriceDTO.getDestLongitude();
        String destLatitude = forecastPriceDTO.getDestLatitude();
        String cityCode = forecastPriceDTO.getCityCode();
        String vehicleType = forecastPriceDTO.getVehicleType();

        return forecastPriceService.forecastPrice(depLongitude,
                depLatitude,
                destLongitude,
                destLatitude,
                cityCode,
                vehicleType);

    }

    /**
     * 计算实际价格
     * @param distance 距离
     * @param duration 时长
     * @param cityCode 城市
     * @param vehicleType 车辆类型
     * @return
     */
    @PostMapping("/calculate-price")
    public ResponseResult calculatePrice(
            @RequestParam Integer distance,
            @RequestParam Integer duration,
            @RequestParam String cityCode,
            @RequestParam String vehicleType) {

        forecastPriceService.calculatePrice(distance, duration, cityCode, vehicleType);
        return null;
    }
}
