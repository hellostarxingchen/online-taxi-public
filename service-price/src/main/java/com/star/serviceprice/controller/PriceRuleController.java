package com.star.serviceprice.controller;

import com.star.internalcommon.dto.PriceRule;
import com.star.internalcommon.dto.ResponseResult;
import com.star.serviceprice.service.PriceRuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

/**
 * @author 星辰
 * @since 2022-12-15
 */

@RestController
@RequestMapping("/price-rule")
public class PriceRuleController {

    @Autowired
    private PriceRuleService priceRuleService;

    //添加计价规则
    @PostMapping("/add")
    public ResponseResult add(@RequestBody PriceRule priceRule){

        return priceRuleService.add(priceRule);
    }

    //编辑计价规则
    @PostMapping("/edit")
    public ResponseResult edit(@RequestBody PriceRule priceRule){

        return priceRuleService.edit(priceRule);
    }

    //根据城市编码和车型查询计价规则
    @PostMapping("/if-exists")
    public ResponseResult ifExists(@RequestBody PriceRule priceRule){

        return priceRuleService.ifExists(priceRule);
    }

}
