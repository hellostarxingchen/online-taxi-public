package com.star.serviceprice.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.star.internalcommon.constant.CommonStatusEnum;
import com.star.internalcommon.dto.PriceRule;
import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.request.ForecastPriceDTO;
import com.star.internalcommon.responese.DirectionResponse;
import com.star.internalcommon.responese.ForecastPriceResponse;
import com.star.internalcommon.util.BigDecimalUtils;
import com.star.serviceprice.mapper.PriceRuleMapper;
import com.star.serviceprice.remote.ServiceMapClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class PriceService {

    @Autowired
    private ServiceMapClient serviceMapClient;

    @Autowired
    private PriceRuleMapper priceRuleMapper;

    //根据 出发地和目的地的经纬度  计算预估价格
    public ResponseResult forecastPrice(String depLongitude,
                                        String depLatitude,
                                        String destLongitude,
                                        String destLatitude,
                                        String cityCode,
                                        String vehicleType){

        log.info("调用地图服务，查询距离和时长");
        ForecastPriceDTO forecastPriceDTO = new ForecastPriceDTO();
        forecastPriceDTO.setDepLongitude(depLongitude);
        forecastPriceDTO.setDepLatitude(depLatitude);
        forecastPriceDTO.setDestLongitude(destLongitude);
        forecastPriceDTO.setDestLatitude(destLatitude);

        ResponseResult<DirectionResponse> direction = serviceMapClient.direction(forecastPriceDTO);
        Integer distance = direction.getData().getDistance();
        Integer duration = direction.getData().getDuration();
        log.info("距离:"+distance+"时长："+duration);

        log.info("读取计价规则");
        Map<String, Object> queryMap = new HashMap<>();
        queryMap.put("city_code",cityCode);
        queryMap.put("vehicle_type",vehicleType);
        //走最近的路线最低的价格
        QueryWrapper wrapper = new QueryWrapper<>();
        wrapper.eq("city_code",cityCode);
        wrapper.eq("vehicle_type",vehicleType);
        wrapper.orderByDesc("fare_version");

        List<PriceRule> priceRules = priceRuleMapper.selectList(wrapper);
        if (priceRules.size() == 0){
            return ResponseResult.fail(CommonStatusEnum.PRICE_RULE_EMPTY.getCode(),
                    CommonStatusEnum.VERIFICATION_CODE_DRROR.getValue());
        }
        PriceRule priceRule = priceRules.get(0);
        log.info("根据距离，时长和计价规则，计算价格");
        double price = getPrice(distance, duration, priceRule);
        System.out.println("费用是："+price);

        ForecastPriceResponse forecastPriceResponse = new ForecastPriceResponse();
        forecastPriceResponse.setPrice(price);
        forecastPriceResponse.setCityCode(cityCode);
        forecastPriceResponse.setVehicleType(vehicleType);

        return ResponseResult.success(forecastPriceResponse);
    }



    /**
     * 根距离和时长计算出最终的价格
     * @param distance 距离
     * @param duration 时长
     * @param priceRule 计价规则
     * @return 最终的价格
     */
    public double getPrice(Integer distance,Integer duration,PriceRule priceRule){
        double price = 0.0;

        //起步价和里程价相加
        double startFare = priceRule.getStartFare();
        price = BigDecimalUtils.add(price,startFare);

        //里程单位换算，换成【km】
        double distanceMile = BigDecimalUtils.divide(distance,1000);
        //起步里程
        double startMile = (double)priceRule.getStartMile();
        double distanceSubtract = BigDecimalUtils.subtract(distanceMile, startMile);
        //最终收费的里程 km
        double mile = distanceSubtract<0?0:distanceSubtract;
        //计算单价  元/km
        Double unitPriceMile = priceRule.getUnitPricePerMile();
        //里程价格
        double mileFare = BigDecimalUtils.multiply(mile,unitPriceMile);
        //起步价 + 里程费【多少钱 / 公里】
        price = BigDecimalUtils.add(price,mileFare);

        //时长
        //时长/ 分钟
        double timeMinute = BigDecimalUtils.divide(duration,60);
        //计时单价
        Double unitPricePerMinute = priceRule.getUnitPricePerMinute();
        //时长费用
        double timeFare = BigDecimalUtils.multiply(timeMinute,unitPricePerMinute);
        //最后加上时长的费用，就是总费用
        price = BigDecimalUtils.add(price,timeFare);

        //设置精度，费用的精确小数位
        BigDecimal priceBgiDecimal = BigDecimal.valueOf(price);
        priceBgiDecimal = priceBgiDecimal.setScale(2,BigDecimal.ROUND_HALF_UP);

        return priceBgiDecimal.doubleValue();

    }

    public ResponseResult calculatePrice(Integer distance, Integer duration, String cityCode, String vehicleType) {
        QueryWrapper wrapper = new QueryWrapper<>();
        wrapper.eq("city_code",cityCode);
        wrapper.eq("vehicle_type",vehicleType);
        wrapper.orderByDesc("fare_version");

        List<PriceRule> priceRules = priceRuleMapper.selectList(wrapper);
        if (priceRules.size() == 0){
            return ResponseResult.fail(
                    CommonStatusEnum.PRICE_RULE_EMPTY.getCode(),
                    CommonStatusEnum.VERIFICATION_CODE_DRROR.getValue());
        }

        PriceRule priceRule = priceRules.get(0);
        log.info("根据距离，时长和计价规则，计算价格");
        double price = getPrice(distance, duration, priceRule);
        log.info("费用是："+price);

        return ResponseResult.success(price);
    }


//    public static void main(String[] args) {
//
//        PriceRule priceRule = new PriceRule();
//        priceRule.setUnitPricePerMile(1.5);
//        priceRule.setUnitPricePerMinute(0.5);
//        priceRule.setStartFare(10.0);
//        priceRule.setStartMile(2);
//
//        System.out.println(getPrice(6500,1800,priceRule));
//    }

}
