package com.star.serviceprice.remote;

import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.request.ForecastPriceDTO;
import com.star.internalcommon.responese.DirectionResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 服务名要和 nacos上的一致
 * 【注意】：不能有 /
 */
@FeignClient("service-map")
public interface ServiceMapClient {

    @RequestMapping(method = RequestMethod.GET,value = "/direction/driving")
    public ResponseResult<DirectionResponse>  direction(@RequestBody ForecastPriceDTO forecastPriceDTO);


}
