package com.star.serviceprice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.star.internalcommon.dto.PriceRule;
import org.springframework.stereotype.Repository;

/**
 * @author 星辰
 * @since 2022-12-15
 */
@Repository
public interface PriceRuleMapper extends BaseMapper<PriceRule> {

}
