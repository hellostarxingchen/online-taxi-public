package com.star.servicessepush.controller;

import com.star.internalcommon.util.SsePrefixUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @Auther: Star
 * @Datea: 2023/1/26  -01  -26
 * @Description: com.star.ssedriverclientweb.controller
 * @version: 1.0
 */
@Slf4j
@RestController
public class SseController {

    public static Map<String,SseEmitter> sseEmitterMap = new HashMap<>();

    @GetMapping("/connect")
    public SseEmitter onet(@RequestParam Long userId,@RequestParam String identity){
        log.info("用户id："+userId+"，身份类型："+identity);
        SseEmitter sseEmitter = new SseEmitter(0l);
        String sseMapKey = SsePrefixUtils.generatorSseKey(userId, identity);
        sseEmitterMap.put(sseMapKey,sseEmitter);
        return sseEmitter;
    }

    @GetMapping("/push")
    public String push(@RequestParam Long userId,@RequestParam String identity,@RequestParam String content){
        log.info("用户id："+userId+"，身份是啥？："+identity);
        String sseMapKey = SsePrefixUtils.generatorSseKey(userId, identity);
        try {
            if (sseEmitterMap.containsKey(sseMapKey)){
                sseEmitterMap.get(sseMapKey).send(content);
            }else {
                return "推送失败";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "给用户："+sseMapKey+"，发送了消息："+content;
    }

    //关闭连接
    @GetMapping("/close")
    public String close(@RequestParam Long userId,@RequestParam String identity){
        String sseMapKey = SsePrefixUtils.generatorSseKey(userId, identity);
        log.info(sseMapKey);
        if (sseEmitterMap.containsKey(sseMapKey)){
            sseEmitterMap.remove(sseMapKey);
        }
        return "close 成功了，哈哈";
    }
}
