package com.star.servicessepush;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
//@EnableFeignClients
public class ServiceSsePushApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceSsePushApplication.class, args);
    }

}
