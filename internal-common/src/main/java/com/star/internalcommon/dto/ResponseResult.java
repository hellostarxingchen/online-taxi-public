package com.star.internalcommon.dto;

import com.star.internalcommon.constant.CommonStatusEnum;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 统一返回结果集
 * @param <T>
 */
@Data
@Accessors(chain = true)   //每次 set完之后，返回的都是一个对象
public class ResponseResult<T> {

    //状态
    private int code;
    //返回信息
    private String message;
    //数据
    private T data;

    //
    public static <T> ResponseResult success(){
        return new ResponseResult()
                .setCode(CommonStatusEnum.SUCCESS.getCode())
                .setMessage(CommonStatusEnum.SUCCESS.getValue());
    }

    /**
     * 成功响应的方法
     * @param data
     * @param <T>
     * @return
     */
    public static <T> ResponseResult success(T data){
        return new ResponseResult()
                .setCode(CommonStatusEnum.SUCCESS.getCode())
                .setMessage(CommonStatusEnum.SUCCESS.getValue())
                .setData(data);
    }

    /**
     * 自定义失败错误提示信息
     * @param code
     * @param message
     * @return
     */
    public static ResponseResult fail(int code,String message){
        return new ResponseResult()
                .setCode(code)
                .setMessage(message);
    }

    /**
     * 统一失败【不建议使用】
     * @param data
     * @param <T>
     * @return
     */
    public static <T> ResponseResult fail(T data){
        return new ResponseResult().setData(data);
    }

    /**
     * 自定义失败，错误码，提示信息【具体错误】
     * @param code
     * @param message
     * @param data
     * @return
     */
    public static ResponseResult fail(int code,String message,String data){
        return new ResponseResult()
                .setCode(code)
                .setMessage(message)
                .setData(data);
    }

}
