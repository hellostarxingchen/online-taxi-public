package com.star.internalcommon.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import java.time.LocalDateTime;

/**
 * @author 星辰
 * @since 2022-11-24
 */
//@TableName("driver_car_binding_relationship")
@Data
public class DriverCarBindingRelationship {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Long driverId;

    private Long carId;

    private Integer bindState;

    private LocalDateTime bindingTime;

    private LocalDateTime unBindingTime;


}
