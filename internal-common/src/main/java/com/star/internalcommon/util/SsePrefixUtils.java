package com.star.internalcommon.util;

/**
 * @Auther: Star
 * @Datea: 2023/1/27  -01  -27
 * @Description: com.star.internalcommon.util
 * @version: 1.0
 */

/**
 * 客户端工具类
 */
public class SsePrefixUtils {

    public static final String sperator = "$";

    //用户 id 和用户的身份 代号使用分隔符分开，例如：用户id：111代号：1 -> 1$11
    public static String generatorSseKey(Long userId,String identity){
        return userId+sperator+identity;
    }
}
