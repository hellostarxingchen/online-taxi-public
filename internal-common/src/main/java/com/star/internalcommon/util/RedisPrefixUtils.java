package com.star.internalcommon.util;

public class RedisPrefixUtils {

    //验证码的前缀
    public static String verificationCodePrefix = "verification-code-";

    //token存储的前缀
    public static String tokenPrefix = "token-";

    //黑名单设备号
    public static final String blackDeviceCodePrefix = "black-device-";

    //根据手机号，生成 key
    public static String generatorKeyPhone(String phone,String identity){
        return verificationCodePrefix +identity+"-"+ phone;
    }

    //根据手机号和身份标识，生成 token
    public static String generatorTokenKey(String phone,String identity,String tokenType){
        return tokenPrefix + phone+"-"+identity+"-"+tokenType;
    }
}
