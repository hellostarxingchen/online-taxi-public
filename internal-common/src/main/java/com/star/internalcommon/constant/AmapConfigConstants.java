package com.star.internalcommon.constant;

/**
 * 地图常量
 */
public class AmapConfigConstants {

    //地图链接，形成全图
    public static final String DIRECTION_URL = "https://restapi.amap.com/v3/direction/driving";

    //行政区域查询
    public static final String DISTRICT_URL = "https://restapi.amap.com/v3/config/district";

    //添加服务
    public static final String SERVER_ADD_URL = "https://tsapi.amap.com/v1/track/service/add";

    //创建终端
    public static final String TERMINAL_ADD = "https://tsapi.amap.com/v1/track/terminal/add";

    // 终端搜索
    public static final String TERMINAL_AROUNDSEARCH = "https://tsapi.amap.com/v1/track/terminal/aroundsearch";

    //创建轨迹
    public static final String TRACK_ADD = "https://tsapi.amap.com/v1/track/trace/add";

    //轨迹点上传
    public static final String POINT_UPLOAD = "https://tsapi.amap.com/v1/track/point/upload";

    //查询轨迹结果（包括：路程和时长）
    public static final String TERMINAL_TRSEARCH = "https://tsapi.amap.com/v1/track/terminal/trsearch";

    //路径规划 json key
    public static final String STATUS = "status";

    //路线
    public static final String ROUTE = "route";

    //JSON 数组
    public static final String PATHS = "paths";

    //
    public static final String DISTANCE = "distance";

    public static final String DURATION = "duration";

    public static final String DISTRICTS = "districts";

    public static final String ADCODE = "adcode";

    public static final String NAME = "name";

    public static final String LEVEL = "level";

    public static final String STREET = "street";
}
