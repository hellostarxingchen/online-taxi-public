package com.star.internalcommon.constant;

public class TokenConstants {

    public static final String REFRESH_TOKEN_TYPE = "refreshToken";

    public static final String ACCESS_TOKEN_TYPE = "accessToken";
}
