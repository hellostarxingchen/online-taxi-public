package com.star.internalcommon.constant;

/**
 * 司机与车辆的状态常量
 */
public class DriverCarConstants {

    /**
     * 绑定状态 1，绑定。2，解绑
     */
    public static final int DRIVER_CAR_BIND = 1;

    public static final int DRIVER_CAR_UNBIND = 2;

    /**
     * 司机状态：1，有效。0无效
     */
    public static int DRIVER_STATE_VALID = 1;

    public static int DRIVER_STATE_INVALID = 0;

    /**
     * 司机状态：存在 1，不存在：0
     */
    public static int DRIVER_EXISTS = 1;

    public static int DRIVER_NOT_EXISTS = 0;

    /**
     *司机工作状态：1：出车，0：收车，2：暂停
     */
    public static int DRIVER_WORK_STATUS_STOP = 0;

    public static int DRIVER_WORK_STATUS_START = 1;

    public static int DRIVER_WORK_STATUS_SUSPEND = 2;

}
