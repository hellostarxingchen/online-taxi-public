package com.star.internalcommon.constant;

import lombok.Getter;

/**
 * 定义一个枚举，返回结果信息，是成功还是失败
 */

public enum CommonStatusEnum {

    /**
     * 错误提示：1000 ~ 1099
     */
    VERIFICATION_CODE_DRROR(109,"验证码不正确"),

    /**
     * 用户提示：
     */
    USER_NOT_EXISTS(1200,"当前用户不存在 "),

    /**
     * 提示信息：13000，计价规则不存在
     */
    PRICE_RULE_EMPTY (1300,"计价规则不存在"),

    PRICE_RULE_EXISTS (1301,"添加失败！计价规则已存在"),

    PRICE_RULE_EDIT (1302,"计价规则没变化哦！"),

    /**
     * Token类 提示：
     */
    TOKEN_ERROR(1199,"token 错误"),

    SUCCESS(1,"success"),
    FAIL(0,"fail"),

    /**
     * 地图信息 1400 ~ 1499
     */

    MAP_DISTRICT_ERROR(1400,"请求地图错误"),

    /**
     *绑定信息
     */
    DRIVER_CAR_BIND_NOT_EXISTS(1500,"车辆和司机绑定关系不存在"),

    DRIVER_NOT_EXISTS(1501,"司机不存在"),

    DRIVER_CAR_BIND_EXISTS(1502,"绑定关系已存在，请勿重复"),

    DRIVER_BIND_EXISTS(1503,"司机已被绑定，请勿重复"),

    CAR_BIND_EXISTS(1504,"车辆已被绑定，请勿重复"),

    CITY_DRIVER_EMPTY(1505,"当前城市没用可用的司机"),

    AVAILABLE_DRIVER_EMPTY(1506,"当前暂无可用的司机"),

    ORDER_GOING_ON(1600,"有正在进行的订单或者有尚未完成支付的订单"),

    /**
     * 新用户优惠，防止刷单
     */
    DEVICE_IS_BLACK(1601,"下单次数已达上限"),

    CITY_SERVICE_NOT_SERVICE(1602,"当前城市不支持叫车服务")

    ;

    @Getter
    private int code;
    @Getter
    private String value;

    CommonStatusEnum(int code, String value) {
        this.code = code;
        this.value = value;
    }
}
