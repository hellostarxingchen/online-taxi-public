package com.star.internalcommon.constant;

import lombok.Data;

/**
 * 订单状态【常量】
 */

@Data
public class OrderConstants {

    //1：乘客下单，订单的开始
    public static final int ORDER_START =1;

    //2：司机开始接单
    public static final int DRIVER_REECIVE_ORDER = 2;

    //3：司机去接乘客
    public static final int DRIVER_TO_PICK_UP_PASSENGER = 3;

    //4：司机到达乘客起点
    public static final int DRIVER_ARRIVED_DEPARTURE = 4;

    //5：乘客上车，司机开始行程
    public static final int PICK_UP_PASSENGER = 5;

    //5：到达目的地，行程结束，待支付
    public static final int PASSENGER_GETOFF = 6;

    //7：发起收款
    public static final int TO_START_PAY = 7;

    //8：支付完成
    public static final int SUCCESS_PAY = 8;

    //9：订单的取消
    public static final int CANCEL = 9;

}
