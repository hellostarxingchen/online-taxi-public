package com.star.internalcommon.constant;

/**
 * 在后面使用公共服务的时候，可能要区别于司机和乘客
 */
public class IdentityConstant {

    //乘客身份
    public static final String PASSENGER_IDENTITY = "1";

    //司机身份
    public static final String DRIVER_IDENTITY = "2";
}
