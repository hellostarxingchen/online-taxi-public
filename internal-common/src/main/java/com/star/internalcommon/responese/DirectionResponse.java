package com.star.internalcommon.responese;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class DirectionResponse {

    //距离
    @JsonProperty(value = "distance")
    private Integer distance;

    //时长
    @JsonProperty(value = "duration")
    private Integer duration;
}
