package com.star.internalcommon.responese;

import lombok.Data;

/**
 * @Auther: Star
 * @Datea: 2022/12/10  -12  -10
 * @Description: com.star.internalcommon.responese
 * @version: 1.0
 */
@Data
public class TrackResponse {

    private String trid;

    private String trname;
}
