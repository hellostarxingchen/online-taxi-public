package com.star.internalcommon.responese;

import lombok.Data;

/**
 * @Auther: Star
 * @Datea: 2023/1/30  -01  -30
 * @Description: com.star.internalcommon.responese
 * @version: 1.0
 */
@Data
public class TrsearchResponse {

    private Long driverMile;
    private Long driverTime;
}
