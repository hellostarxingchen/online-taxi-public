package com.star.internalcommon.responese;

import lombok.Data;

@Data
public class NumberCodeResponse {

    private int numberCode;

}
