package com.star.internalcommon.responese;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ForecastPriceResponse {

    private double price;

    private String cityCode;

    private String vehicleType;
}
