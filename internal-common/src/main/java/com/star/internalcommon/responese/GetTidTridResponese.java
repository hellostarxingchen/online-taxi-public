package com.star.internalcommon.responese;

import lombok.Data;

/**
 * @Auther: Star
 * @Datea: 2023/2/20  -02  -20
 * @Description: com.star.internalcommon.responese
 * @version: 1.0
 */
@Data
public class GetTidTridResponese {

    private String tid;

    private String trid;
}
