package com.star.internalcommon.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ForecastPriceDTO {

    //出发地的经纬度
    @JsonProperty(value = "depLongitude")
    private String depLongitude;

    @JsonProperty(value = "depLatitude")
    private String depLatitude;

    //目的地的经纬度
    @JsonProperty(value = "destLongitude")
    private String destLongitude;

    @JsonProperty(value = "destLatitude")
    private String destLatitude;

    @JsonProperty(value = "cityCode")
    private String cityCode;

    @JsonProperty(value = "vehicleType")
    private String vehicleType;
}
