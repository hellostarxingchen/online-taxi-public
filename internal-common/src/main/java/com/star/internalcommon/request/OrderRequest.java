package com.star.internalcommon.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.time.LocalDateTime;

/**
 * 创建订单请求参数
 */
@Data
public class OrderRequest {

    //订单的ID
    private Long orderId;

    //乘客 ID
    private Long passengerId;

    //乘客手机号
    private String passengerPhone;

    //下单的行政区域
    private String address;

    //出发时间
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime departTime;

    //下单时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private LocalDateTime orderTime;

    //出发地
    private String departure;

    //出发地的经纬度
    private String depLongitude;

    private String depLatitude;

    //目的地
    private String destination;

    //目的地的经纬度
    private String destLongitude;

    private String destLatitude;

    //坐标加密表示 1：gcj-02, 2：wgs84,3：bd-09,4：cgcs2000北斗，0：其他
    private Integer encrypt;

    //运价类型编码
    private String fareType;

    //运价版本
    private Integer fareVersion;

    //请求设备唯一码
    private String deviceCode;

    /**
     * 司机去接乘客出发时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime toPickUpPassengerTime;

    /**
     * 去接乘客时，司机的经度
     */
    private String toPickUpPassengerLongitude;

    /**
     * 去接乘客时，司机的纬度
     */
    private String toPickUpPassengerLatitude;

    /**
     * 去接乘客时，司机的地点
     */
    private String toPickUpPassengerAddress;

    /**
     * 接到乘客，乘客上车经度
     */
    private String pickUpPassengerLongitude;

    /**
     * 接到乘客，乘客上车纬度
     */
    private String pickUpPassengerLatitude;

    /**
     * 乘客下车时间
     */
    private LocalDateTime passengerGetoffTime;

    /**
     * 乘客下车经度
     */
    private String passengerGetoffLongitude;

    /**
     * 乘客下车纬度
     */
    private String passengerGetoffLatitude;

}
