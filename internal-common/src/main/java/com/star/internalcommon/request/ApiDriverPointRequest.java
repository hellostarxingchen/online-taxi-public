package com.star.internalcommon.request;

import lombok.Data;

/**
 * @Auther: Star
 * @Datea: 2022/12/12  -12  -12
 * @Description: com.star.internalcommon.request
 * @version: 1.0
 */
@Data
public class ApiDriverPointRequest {

    private Long carId;

    private PointDTO[] points;
}
