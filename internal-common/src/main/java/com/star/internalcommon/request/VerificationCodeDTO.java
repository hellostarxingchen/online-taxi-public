package com.star.internalcommon.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 请求的参数，请求验证手机号码和验证码。并进行对比。
 * 方法名要和接口中的参数要一致
 */
@Data
public class VerificationCodeDTO {

    //乘客校验手机号和验证码
    @JsonProperty(value = "passengerPhone")
    private String passengerPhone;

    @JsonProperty(value = "verificationCode")
    private String verificationCode;

    //司机校验手机号
//    @JsonProperty(value = "driverPhone")
    private String driverPhone;

}
