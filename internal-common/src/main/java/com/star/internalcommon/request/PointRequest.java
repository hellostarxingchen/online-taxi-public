package com.star.internalcommon.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Auther: Star
 * @Datea: 2022/12/12  -12  -12
 * @Description: com.star.internalcommon.request
 * @version: 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PointRequest {

    private String tid;

    private String trid;

    private PointDTO[] points;
}
