package com.star.serviceverificationcode.controller;

import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.responese.NumberCodeResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * 这个层次的接口要和Apifox中的接口要一一对应，最好是复制过来
 */
@RestController
public class NumberCodeController {

    @GetMapping("/numberCode/{size}")
    public ResponseResult numberCode(@PathVariable("size") int size){
        //生成验证码
        double d = (Math.random()*9 + 1) * (Math.pow(10,size-1));
        int resultInt = (int)d;

        System.out.println("随机数->"+resultInt);

        NumberCodeResponse response = new NumberCodeResponse();
        response.setNumberCode(resultInt);

        return ResponseResult.success(response);
    }

}
