package com.star.servicemap.service;

import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.responese.DirectionResponse;
import com.star.servicemap.remote.MapDirectionClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DirectionService {

    @Autowired
    private MapDirectionClient mapDirectionClient;

    public ResponseResult driving(String depLongitude,
                                  String depLatitude,
                                  String destLongitude,
                                  String destLatitude){

        DirectionResponse direction = mapDirectionClient.
                direction(depLongitude, depLatitude, destLongitude, destLatitude);

        return ResponseResult.success(direction);
    }
}
