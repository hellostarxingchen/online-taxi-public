package com.star.servicemap.service;

import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.request.PointRequest;
import com.star.servicemap.remote.PointClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @Auther: Star
 * @Datea: 2022/12/12  -12  -12
 * @Description: com.star.servicemap.service
 * @version: 1.0
 */
@Service
public class PointService {

    @Autowired
    private PointClient pointClient;

    public ResponseResult upload(@RequestBody PointRequest pointRequest){

        return pointClient.upload(pointRequest);
    }
}
