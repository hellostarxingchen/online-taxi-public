package com.star.servicemap.service;

import com.star.internalcommon.dto.ResponseResult;
import com.star.servicemap.remote.TrackClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Auther: Star
 * @Datea: 2022/12/10  -12  -10
 * @Description: com.star.servicemap.service
 * @version: 1.0
 */
@Service
public class TrackService {

    @Autowired
    private TrackClient trackClient;

    public ResponseResult addTrack(String tid){

        return trackClient.addTrack(tid);
    }
}
