package com.star.servicemap.service;

import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.responese.TerminalResponse;
import com.star.internalcommon.responese.TrsearchResponse;
import com.star.servicemap.remote.TerminalClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TerminalService {

    @Autowired
    private TerminalClient terminalService;

    public ResponseResult<TerminalResponse> add(String name,String desc){

        return terminalService.add(name,desc);
    }

    public ResponseResult<List<TerminalResponse>> aroundsearch(String center, Integer radius){

        return terminalService.aroundsearch(center, radius);
    }

    public ResponseResult<TrsearchResponse> trsearch(String tid, Long starttime, Long endtime){

        return terminalService.trsearch(tid,starttime,endtime);
    }
}
