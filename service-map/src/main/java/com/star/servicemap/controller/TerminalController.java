package com.star.servicemap.controller;

import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.responese.TerminalResponse;
import com.star.internalcommon.responese.TrsearchResponse;
import com.star.servicemap.service.TerminalService;
import org.apache.ibatis.javassist.runtime.Desc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/terminal")
public class TerminalController {

    @Autowired
    private TerminalService terminalService;

    //创建终端
    @PostMapping("/add")
    public ResponseResult<TerminalResponse> add(String name,String desc){
        return terminalService.add(name,desc);
    }

    //搜索终端
    @PostMapping("/aroundsearch")
    public ResponseResult<List<TerminalResponse>> aroundsearch(String center, Integer radius){

        return terminalService.aroundsearch(center, radius);
    }

    //查询轨迹
    @PostMapping("/trsearch")
//    @GetMapping("/trsearch")
    public ResponseResult<TrsearchResponse> trsearch(String tid, Long starttime, Long endtime){

        return terminalService.trsearch(tid,starttime,endtime);
    }

}
