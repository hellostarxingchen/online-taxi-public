package com.star.servicemap.controller;

import com.star.internalcommon.dto.ResponseResult;
import com.star.servicemap.service.TrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Auther: Star
 * @Datea: 2022/12/10  -12  -10
 * @Description: com.star.servicemap.controller
 * @version: 1.0
 */
@RestController
@RequestMapping("/track")
public class TrackController {

    @Autowired
    private TrackService trackService;

    @PostMapping("/add")
    public ResponseResult addTrack(String tid){

        return trackService.addTrack(tid);
    }
}
