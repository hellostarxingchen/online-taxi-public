package com.star.servicemap.controller;

import com.star.internalcommon.dto.DicDistrict;
import com.star.servicemap.mapper.DicDistrictMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

@RestController
public class TestController {

    @Autowired
    private DicDistrictMapper dicDistrictMapper;

    @GetMapping("/ok")
    public String testTest(){
        HashMap<String, Object> map = new HashMap<>();
        map.put("address_code","110000");
        List<DicDistrict> dicDistricts = dicDistrictMapper.selectByMap(map);
        System.out.println(dicDistricts);
        return "test1100";
    }
}
