package com.star.servicemap.controller;

import com.star.internalcommon.dto.ResponseResult;
import com.star.servicemap.service.DicDistrictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DistrictController {

    @Autowired
    private DicDistrictService dicDistrictService;

    @GetMapping("/dic-district")
    public ResponseResult initDicDistrict(String keywords) {

        return dicDistrictService.dicDistrict(keywords);
    }
}
