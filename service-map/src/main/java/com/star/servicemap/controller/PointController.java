package com.star.servicemap.controller;

import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.request.PointRequest;
import com.star.servicemap.service.PointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Auther: Star
 * @Datea: 2022/12/12  -12  -12
 * @Description: com.star.servicemap.controller
 * @version: 1.0
 */
@RestController
@RequestMapping("/point")
public class PointController {

    @Autowired
    private PointService pointService;

    @PostMapping("/upload")
    public ResponseResult upload(@RequestBody PointRequest pointRequest){

        return pointService.upload(pointRequest);
    }
}
