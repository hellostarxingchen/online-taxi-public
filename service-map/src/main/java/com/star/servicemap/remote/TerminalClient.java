package com.star.servicemap.remote;

import com.star.internalcommon.constant.AmapConfigConstants;
import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.responese.TerminalResponse;
import com.star.internalcommon.responese.TrsearchResponse;
import com.sun.deploy.net.HttpUtils;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.http.client.HttpClient;
import org.apache.http.client.utils.HttpClientUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.swing.plaf.metal.MetalIconFactory;
import java.awt.geom.Area;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class TerminalClient {

    @Value("${amap.key}")
    public String amapKey;
    @Value("${amap.sid}")
    public String amapSid;

    @Autowired
    private RestTemplate restTemplate;

    //创建终端
    public ResponseResult<TerminalResponse> add(String name, String desc) {

        StringBuilder url = new StringBuilder();
        url.append(AmapConfigConstants.TERMINAL_ADD);
        url.append("?");
        url.append("key=" + amapKey + "&");
        url.append("sid=" + amapSid + "&");
        url.append("name=" + name + "&");
        url.append("desc=" + desc);

        System.out.println("创建终端请求：" + url.toString());
        ResponseEntity<String> stringResponseEntity = restTemplate.postForEntity(url.toString(), null, String.class);
        System.out.println("创建终端响应：" + stringResponseEntity.getBody());

        //把从API上得到的结果转为 json，并将这个结果返回给
        String body = stringResponseEntity.getBody();
        JSONObject result = JSONObject.fromObject(body);
        JSONObject data = result.getJSONObject("data");
        String tid = data.getString("tid");

        TerminalResponse terminalResponse = new TerminalResponse();
        terminalResponse.setTid(tid);
        return ResponseResult.success(terminalResponse);
    }

    //终端搜索
    public ResponseResult<List<TerminalResponse>> aroundsearch(String center, Integer radius) {
        StringBuilder url = new StringBuilder();
        url.append(AmapConfigConstants.TERMINAL_AROUNDSEARCH);
        url.append("?");
        url.append("key=" + amapKey + "&");
        url.append("sid=" + amapSid + "&");
        url.append("center=" + center + "&");
        url.append("radius=" + radius);
        System.out.println("终端搜索请求：" + url.toString());
        ResponseEntity<String> stringResponseEntity = restTemplate.postForEntity(url.toString(), null, String.class);
        System.out.println("终端搜索响应：" + stringResponseEntity.getBody());

        // 解析终端搜索结果
        String body = stringResponseEntity.getBody();
        JSONObject result = JSONObject.fromObject(body);
        JSONObject data = result.getJSONObject("data");

        List<TerminalResponse> terminalResponseList = new ArrayList<>();

        JSONArray results = data.getJSONArray("results");
        for (int i = 0; i < results.size(); i++) {
            TerminalResponse terminalResponse = new TerminalResponse();

            JSONObject jsonObject = results.getJSONObject(i);
            // desc是 carId，
            String desc = jsonObject.getString("desc");
            Long carId = Long.parseLong(desc);
            String tid = jsonObject.getString("tid");

            JSONObject location = jsonObject.getJSONObject("location");
//            long longitude = location.getLong("longitude");
//            long latitude = location.getLong("latitude");
            String longitude = location.getString("longitude");
            String latitude = location.getString("latitude");

            terminalResponse.setCarId(carId);
            terminalResponse.setTid(tid);
            terminalResponse.setLongitude(longitude);
            terminalResponse.setLatitude(latitude);

            terminalResponseList.add(terminalResponse);
        }


        return ResponseResult.success(terminalResponseList);
    }

    //搜索轨迹
    public ResponseResult<TrsearchResponse> trsearch(String tid, Long starttime, Long endtime) {
        StringBuilder url = new StringBuilder();
        url.append(AmapConfigConstants.TERMINAL_TRSEARCH);
        url.append("?");
        url.append("key=" + amapKey + "&");
        url.append("sid=" + amapSid + "&");
        url.append("tid=" + tid + "&");
        url.append("starttime=" + starttime + "&");
        url.append("endtime=" + endtime);

        log.info("高德地图查询轨迹的请求：" + url.toString());
        ResponseEntity<String> forEntity = restTemplate.getForEntity(url.toString(), String.class);
        log.info("高德地图查询轨迹的响应：" + forEntity.getBody());

        JSONObject result = JSONObject.fromObject(forEntity.getBody());
        JSONObject data = result.getJSONObject("data");
        int counts = data.getInt("counts");
        if (counts == 0){
            return null;
        }
        //从响应结果集中分别取出各种属性
        JSONArray tracks = data.getJSONArray("tracks");
        long driveMile = 0L;
        long driveTime = 0L;
        for (int i = 0; i < tracks.size(); i++) {
            JSONObject jsonObject = tracks.getJSONObject(i);
            long distance = jsonObject.getLong("distance");
            driveMile = driveMile + distance; //距离累加
            long time = jsonObject.getLong("time");
            time = time / (1000 * 60);
            driveTime = driveTime + time;  //时间累加

        }
        TrsearchResponse trsearchResponse = new TrsearchResponse();
        trsearchResponse.setDriverMile(driveMile);
        trsearchResponse.setDriverTime(driveTime);

        return ResponseResult.success(trsearchResponse);
    }
}
