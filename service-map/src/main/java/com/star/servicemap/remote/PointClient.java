package com.star.servicemap.remote;

import com.star.internalcommon.constant.AmapConfigConstants;
import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.request.PointDTO;
import com.star.internalcommon.request.PointRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

/**
 * @Auther: Star
 * @Datea: 2022/12/12  -12  -12
 * @Description: com.star.servicemap.remote
 * @version: 1.0
 */
@Service
public class PointClient {

    @Value("${amap.key}")
    private String amapKey;

    @Value("${amap.sid}")
    private String amapSid;

    @Autowired
    private RestTemplate restTemplate;

    //上传轨迹信息
    public ResponseResult upload(@RequestBody PointRequest pointRequest){

        StringBuilder url = new StringBuilder();
        url.append(AmapConfigConstants.POINT_UPLOAD);
        url.append("?");
        url.append("key=" + amapKey+"&");
        url.append("sid=" + amapSid+"&");
        url.append("tid="+pointRequest.getTid()+"&");
        url.append("trid="+pointRequest.getTrid()+"&");
        url.append("points=");
        PointDTO[] points = pointRequest.getPoints();
        //这个是符号编码
        url.append("%5B");
        for (PointDTO p : points) {
            url.append("%7B");
            Long locatetime = p.getLocatetime();
            String location = p.getLocation();
            url.append("%22location%22");
            url.append("%3A");
            url.append("%22"+location+"%22");
            url.append("%2C");

            url.append("%22locatetime%22");
            url.append("%3A");
            url.append(locatetime);

            url.append("%7D");
        }
        url.append("%5D");

        System.out.println("上传位置请求："+url.toString());
        ResponseEntity<String> stringResponseEntity = restTemplate.postForEntity(URI.create(url.toString()), null, String.class);
        System.out.println("上传位置响应："+stringResponseEntity.getBody());

        return ResponseResult.success();
    }
}
