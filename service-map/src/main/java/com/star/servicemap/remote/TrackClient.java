package com.star.servicemap.remote;

import com.star.internalcommon.constant.AmapConfigConstants;
import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.responese.TrackResponse;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @Auther: Star
 * @Datea: 2022/12/10  -12  -10
 * @Description: com.star.servicemap.remote
 * @version: 1.0
 */
@Service
@Slf4j
public class TrackClient {

    @Value("${amap.key}")
    public String amapKey;
    @Value("${amap.sid}")
    public String amapSid;

    @Autowired
    private RestTemplate restTemplate;

    public ResponseResult<TrackResponse> addTrack(String tid){

        StringBuilder url = new StringBuilder();
        url.append(AmapConfigConstants.TRACK_ADD);
        url.append("?");
        url.append("key=" + amapKey+"&");
        url.append("sid=" + amapSid+"&");
        url.append("tid="+tid);
        log.info("高德地图创建轨迹请求："+url);
        ResponseEntity<String> stringResponseEntity = restTemplate.postForEntity(url.toString(), null, String.class);
        String body = stringResponseEntity.getBody();
        log.info("高德地图创建轨迹响应："+body);
        JSONObject result = JSONObject.fromObject(body);
        JSONObject data = result.getJSONObject("data");
        String trid = data.getString("trid");

        //轨迹名称
        String trname = null;
        if (data.has("trname")){
            trname = data.getString("trname");
        }

        TrackResponse trackResponse = new TrackResponse();
        trackResponse.setTrid(trid);
        trackResponse.setTrname(trname);

        return ResponseResult.success(trackResponse);
    }
}
