package com.star.servicemap.remote;

import java.lang.String;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.fasterxml.jackson.databind.util.JSONWrappedObject;
import com.star.internalcommon.constant.AmapConfigConstants;
import com.star.internalcommon.responese.DirectionResponse;
import lombok.extern.slf4j.Slf4j;
import org.kordamp.json.JSONArray;
import org.kordamp.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class MapDirectionClient {

    //接收来自yml配置中的数据
    @Value("${amap.key}")
    private String amapKey;

    @Autowired
    private RestTemplate restTemplate;

    public DirectionResponse direction(String depLongitude,
                                       String depLatitude,
                                       String destLongitude,
                                       String destLatitude) {

        //组装请求调用 url
        /**
         * ?origin=116.481028,39.989643&destination=116.465302,40.004717&city=010&output=json&4c6d851411adff0286ed7d958f2e030c
         */
        StringBuilder urlBuilder = new StringBuilder();
        urlBuilder.append(AmapConfigConstants.DIRECTION_URL);
        urlBuilder.append("?");
        urlBuilder.append("origin=" + depLongitude + "," + depLatitude+"&");
        urlBuilder.append("destination=" + destLongitude + "," + destLatitude+"&");
        urlBuilder.append("extensions=base"+"&");
        urlBuilder.append("output=json"+"&");
        urlBuilder.append("key=" + amapKey);
        log.info(urlBuilder.toString());

        //调用高德接口
        ResponseEntity<String> directionEntity = restTemplate.getForEntity(urlBuilder.toString(), String.class);
        String directionString = directionEntity.getBody();
        log.info("高地图" + directionString);

        //解析接口【单独的方法】
        DirectionResponse directionResponse = pareDirectionEntity(directionString);

        return directionResponse;
    }

    private DirectionResponse pareDirectionEntity(String directionString) {
        DirectionResponse directionResponse = null;

        try {

            //最外层
            JSONObject result = JSONObject.fromObject(directionString);
            if (result.has(AmapConfigConstants.STATUS)) {
                int status = result.getInt(AmapConfigConstants.STATUS);
                if (status == 1) {
                    if (result.has(AmapConfigConstants.STATUS)) {
                        JSONObject jsonObject = result.getJSONObject(AmapConfigConstants.ROUTE);
                        JSONArray pathArray = jsonObject.getJSONArray(AmapConfigConstants.PATHS);
                        JSONObject pathObject = pathArray.getJSONObject(0);
                        directionResponse = new DirectionResponse();
                        if (pathObject.has(AmapConfigConstants.DISTANCE)) {
                            int distance = pathObject.getInt(AmapConfigConstants.DISTANCE);
                            directionResponse.setDistance(distance);
                        }
                        if (pathObject.has(AmapConfigConstants.DURATION)) {
                            int duration = pathObject.getInt(AmapConfigConstants.DURATION);
                            directionResponse.setDuration(duration);

                        }
                    }
                }
            }
        }catch (Exception e) {

        }
        //把时间和距离解析出来

        return directionResponse;
    }
}
