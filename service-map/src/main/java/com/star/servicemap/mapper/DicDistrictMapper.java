package com.star.servicemap.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.star.internalcommon.dto.DicDistrict;
import org.springframework.stereotype.Repository;

@Repository
public interface DicDistrictMapper extends BaseMapper<DicDistrict> {
}
