package com.star.servicedriveruser.generator;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.Collections;

/**
 * 自动生成代码的工具类
 */
public class MysqlGenerator {

    public static void main(String[] args) {
        FastAutoGenerator.create("jdbc:mysql://localhost:3306/service_driver_user?characterEncoding=utf-8&serverTimezone=GMT%2B8",
                "root","123456")
                .globalConfig(builder -> {
                    builder.author("星辰").fileOverride().outputDir("C:\\Users\\online-taxi-public\\service-driver-user\\src\\main\\java");
                })
                .packageConfig(builder ->{
                    builder.parent("com.star.serviceDriverUser").pathInfo(Collections.singletonMap(OutputFile
                            .mapperXml,"C:\\Users\\online-taxi-public\\service-driver-user\\src\\main\\java\\com\\star\\servicedriveruser\\mapper"));

                })
                .strategyConfig(builder -> {
                    //指定生成代码的数据表
                    builder.addInclude("driver_user_work_status");
                })
                .templateEngine(new FreemarkerTemplateEngine())
                .execute();
    }
}
