package com.star.servicedriveruser.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.star.internalcommon.dto.Car;
import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.responese.GetTidTridResponese;
import com.star.internalcommon.responese.TerminalResponse;
import com.star.internalcommon.responese.TrackResponse;
import com.star.servicedriveruser.mapper.CarMapper;
import com.star.servicedriveruser.remote.ServiceMapClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

@Service
public class CarService {

    @Autowired
    private ServiceMapClient serviceMapClient;

    @Autowired
    private CarMapper carMapper;

    //添加一辆车的轨迹信息
    public ResponseResult addCar(@RequestBody Car car){

        LocalDateTime now = LocalDateTime.now();
        car.setGmtModified(now);
        car.setGmtCreate(now);
        carMapper.insert(car);

        //获取此车辆对应 id 的 【tid】
        ResponseResult<TerminalResponse> responseResult = serviceMapClient.addTerminal(car.getVehicleNo(), car.getId()+ "");
        String tid = responseResult.getData().getTid();
        car.setTid(tid);

        //获得此车辆的轨迹 id 【trid】
        ResponseResult<TrackResponse> terminalResponseResponseResult = serviceMapClient.addTrack(tid);
        String trid = terminalResponseResponseResult.getData().getTrid();
        String trname = terminalResponseResponseResult.getData().getTrname();

        car.setTrid(trid);
        car.setTrname(trname);
        carMapper.updateById(car);

        return ResponseResult.success("");
    }

    //查车辆的轨迹信息
    public ResponseResult<Car> getCarById(Long id){
        HashMap<String, Object> map = new HashMap<>();
        map.put("id",id);
        List<Car> cars = carMapper.selectByMap(map);
        return ResponseResult.success(cars.get(0));
    }

    public GetTidTridResponese getCarTidById(Long carId) {
        Car car = carMapper.selectById(carId);
        String tid = car.getTid();
        String trid = car.getTrid();

        GetTidTridResponese tidResponese = new GetTidTridResponese();
        tidResponese.setTid(tid);
        tidResponese.setTrid(trid);
        return tidResponese;
    }
}
