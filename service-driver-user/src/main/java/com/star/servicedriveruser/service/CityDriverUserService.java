package com.star.servicedriveruser.service;

import com.star.internalcommon.dto.ResponseResult;
import com.star.servicedriveruser.mapper.DriverUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Auther: Star
 * @Datea: 2022/12/17  -12  -17
 * @Description: com.star.servicedriveruser.service
 * @version: 1.0
 */
@Service
public class CityDriverUserService {

    @Autowired
    private DriverUserMapper driverUserMapper;

    public ResponseResult<Boolean> isAvailableDriver(String cityCode){
        int i = driverUserMapper.selectDriverUserCountCityCode(cityCode);
        if (i>0){
            return ResponseResult.success(true);
        }else {
            return ResponseResult.success(false);
        }

    }
}
