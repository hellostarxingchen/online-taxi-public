package com.star.servicedriveruser.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.star.internalcommon.dto.Car;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 星辰
 * @since 2022-11-24
 */
@Repository
public interface CarMapper extends BaseMapper<Car> {


}
