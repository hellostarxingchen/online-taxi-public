package com.star.servicedriveruser.mapper;

import com.star.internalcommon.dto.DriverUserWorkStatus;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * @author 星辰
 * @since 2022-11-29
 */
@Repository
public interface DriverUserWorkStatusMapper extends BaseMapper<DriverUserWorkStatus> {

}
