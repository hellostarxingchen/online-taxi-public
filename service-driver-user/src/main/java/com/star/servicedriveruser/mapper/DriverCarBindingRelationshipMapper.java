package com.star.servicedriveruser.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.star.internalcommon.dto.DriverCarBindingRelationship;
import org.springframework.stereotype.Repository;

/**
 * @author 星辰
 * @since 2022-11-24
 */
@Repository
public interface DriverCarBindingRelationshipMapper extends BaseMapper<DriverCarBindingRelationship> {

}
