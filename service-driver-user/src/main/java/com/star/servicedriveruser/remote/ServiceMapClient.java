package com.star.servicedriveruser.remote;

import com.star.internalcommon.dto.Car;
import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.request.PointRequest;
import com.star.internalcommon.responese.TerminalResponse;
import com.star.internalcommon.responese.TrackResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient("service-map")
public interface ServiceMapClient {

    @RequestMapping(method = RequestMethod.POST,value = "/terminal/add")
    public ResponseResult<TerminalResponse> addTerminal(@RequestParam String name, @RequestParam String desc);

    @RequestMapping(method = RequestMethod.POST,value = "/track/add")
    public ResponseResult<TrackResponse> addTrack(@RequestParam String tid);

    @GetMapping("/car")
    public ResponseResult<Car> getCarTidById(@RequestBody PointRequest pointRequest);
}
