package com.star.servicedriveruser.controller;

import com.star.servicedriveruser.mapper.DriverUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Auther: Star
 * @Datea: 2022/12/17  -12  -17
 * @Description: com.star.servicedriveruser.controller
 * @version: 1.0
 */
@RestController
public class TeController {

    @Autowired
    private DriverUserMapper driverUserMapper;

    @GetMapping("/test-xml")
    public int testXml(String cityCode){
        return driverUserMapper.selectDriverUserCountCityCode(cityCode);
    }
}
