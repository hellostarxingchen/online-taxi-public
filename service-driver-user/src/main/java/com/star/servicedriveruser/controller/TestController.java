package com.star.servicedriveruser.controller;

import com.star.internalcommon.dto.ResponseResult;
import com.star.servicedriveruser.service.DriverUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Autowired
    private DriverUserService driverUserService;

    @GetMapping("/test")
    public ResponseResult test() {
        return driverUserService.test();
    }
}
