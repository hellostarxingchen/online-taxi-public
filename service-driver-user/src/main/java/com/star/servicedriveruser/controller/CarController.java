package com.star.servicedriveruser.controller;

import com.star.internalcommon.dto.Car;
import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.responese.GetTidTridResponese;
import com.star.servicedriveruser.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.time.LocalDateTime;

/**
 * @author 星辰
 * @since 2022-11-24
 */
@RestController
public class CarController {

    @Autowired
    private CarService carService;

    @PostMapping("/car")
    public ResponseResult addCar(@RequestBody Car car) {
        LocalDateTime now = LocalDateTime.now();
        car.setGmtCreate(now);
        car.setGmtModified(now);

        return carService.addCar(car);
    }

    @GetMapping("/car")
    public ResponseResult<Car> getCarById(Long carId){

        return carService.getCarById(carId);
    }

    @PostMapping("/cartid")
    public ResponseResult<GetTidTridResponese> getCarTidById(Long carId){
        GetTidTridResponese carTidById = carService.getCarTidById(carId);
        return ResponseResult.success(carTidById);
    }

}
