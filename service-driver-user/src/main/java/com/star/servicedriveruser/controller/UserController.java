package com.star.servicedriveruser.controller;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.star.internalcommon.constant.DriverCarConstants;
import com.star.internalcommon.dto.DriverCarBindingRelationship;
import com.star.internalcommon.dto.DriverUser;
import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.responese.DriverUserExistsResponse;
import com.star.servicedriveruser.service.DriverUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
public class UserController {

    @Autowired
    private DriverUserService driverUserService;

    //添加司机
    @PostMapping("/user")
    public ResponseResult addUser(@RequestBody DriverUser driverUser){
        log.info("this is a service-driver-user,ok! Star");
        return driverUserService.addDriverUser(driverUser);
    }

    //更新司机
    @PutMapping("/user")
    public ResponseResult updateUser(@RequestBody DriverUser driverUser){
        log.info("修改的司机信息！");
        return driverUserService.updateDriverUser(driverUser);
    }

    //根据手机号查询司机
    @GetMapping("/check-driver/{driverPhone}")
    public ResponseResult<DriverUserExistsResponse> getUser(@PathVariable("driverPhone") String driverPhone){
        ResponseResult<DriverUser> driverUserByPhone = driverUserService.getDriverUserByPhone(driverPhone);
        DriverUser driverPhoneDB =  driverUserByPhone.getData();
        DriverUserExistsResponse response = new DriverUserExistsResponse();
        int ifExists = DriverCarConstants.DRIVER_EXISTS;
        if (driverPhoneDB == null){
            ifExists = DriverCarConstants.DRIVER_NOT_EXISTS;
            response.setDriverPhone(driverPhone);
            response.setIfExists(ifExists);
        }else {
            response.setDriverPhone(driverPhoneDB.getDriverPhone());
            response.setIfExists(ifExists);
        }
        return ResponseResult.success(response);
    }

    //根据车辆ID查询订单需要的司机信息，泛型是车辆司机绑定的实体类
    @GetMapping("/get-available-driver/{carId}")
    public ResponseResult<DriverCarBindingRelationship> getAvailableDriver(@PathVariable("carId") Long carId){

        return driverUserService.getAvailableDriver(carId);
    }
}
