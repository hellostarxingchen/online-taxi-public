package com.star.servicedriveruser.controller;


import com.star.internalcommon.dto.DriverUserWorkStatus;
import com.star.internalcommon.dto.ResponseResult;
import com.star.servicedriveruser.service.DriverUserWorkStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 星辰
 * @since 2022-11-29
 */
@RestController
public class DriverUserWorkStatusController {

    @Autowired
    private DriverUserWorkStatusService driverUserWorkStatusService;

    @PostMapping("/driver-user-work-status")
    public ResponseResult changeWorkStatus(@RequestBody DriverUserWorkStatus driverUserWorkStatus){
       return driverUserWorkStatusService.changeWorkStatus(
               driverUserWorkStatus.getDriverId(),
               driverUserWorkStatus.getWorkStatus());
//        return ResponseResult.success();
    }
}
