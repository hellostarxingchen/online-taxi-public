package com.star.servicedriveruser.controller;

import com.star.internalcommon.dto.ResponseResult;
import com.star.servicedriveruser.service.CityDriverUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Auther: Star
 * @Datea: 2022/12/17  -12  -17
 * @Description: com.star.servicedriveruser.controller
 * @version: 1.0
 */
@RestController
@RequestMapping("/city-driver")
public class CityDriverController {

    @Autowired
    private CityDriverUserService cityDriverUserService;

    //查询当前城市是否有司机
    @GetMapping("/is-alailable-driver")
    public ResponseResult isAvailableDriver(String cityCode){
        return cityDriverUserService.isAvailableDriver(cityCode);
    }
}
