package com.star.servicepassengeruser.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.star.internalcommon.dto.PassengerUser;
import org.springframework.stereotype.Repository;

@Repository
public interface PassengerUserMapper extends BaseMapper<PassengerUser> {
}
