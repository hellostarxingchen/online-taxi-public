package com.star.servicepassengeruser.contrller;

import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.request.VerificationCodeDTO;
import com.star.servicepassengeruser.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/user")
    public ResponseResult loginOrRegister(@RequestBody VerificationCodeDTO verificationCodeDTO){
        String phone = verificationCodeDTO.getPassengerPhone();
        System.out.println("手机号"+phone);
        return userService.loginOrRegister(phone);
    }

    @GetMapping("/user/{phone}")
    public ResponseResult getUser(@PathVariable ("phone") String passengerPhone){
        System.out.println("service-passenger-user: phone--->>>"+passengerPhone);
        return userService.getUserByPhone(passengerPhone);
    }

}
