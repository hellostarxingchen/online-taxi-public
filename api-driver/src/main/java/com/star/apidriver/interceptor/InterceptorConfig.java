package com.star.apidriver.interceptor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 将拦截器注入到容器中
 */
@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

    /**
     * 拦截器是在 Spring的Bean实例化之前进行的初始化
     * 所以要在拦截器初始化的时候，就已经初始化 Bean了
     */
    @Bean
    public JwtInterceptor jwtInterceptor() {
        return new JwtInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //添加之前写的拦截器【new的形式添加】
        registry.addInterceptor(jwtInterceptor())
                //拦截的路径
                .addPathPatterns("/**")
                //放行的路径
                .excludePathPatterns("/noauth")
                .excludePathPatterns("/verification-code")
                .excludePathPatterns("/verification-code-check")
                .excludePathPatterns("/user")
                .excludePathPatterns("/point/upload");

    }
}
