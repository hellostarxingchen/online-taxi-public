package com.star.apidriver.service;

import com.star.apidriver.remote.ServiceDriverUserClient;
import com.star.internalcommon.dto.DriverUser;
import com.star.internalcommon.dto.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private ServiceDriverUserClient serviceDriverUserClient;

    public ResponseResult updateUser(DriverUser driverUser){
        return serviceDriverUserClient.updateUser(driverUser);
    }
}
