package com.star.apidriver.service;

import com.alibaba.cloud.commons.lang.StringUtils;
import com.star.apidriver.remote.ServiceDriverUserClient;
import com.star.apidriver.remote.ServiceVerificationCodeClient;
import com.star.internalcommon.constant.CommonStatusEnum;
import com.star.internalcommon.constant.DriverCarConstants;
import com.star.internalcommon.constant.IdentityConstant;
import com.star.internalcommon.constant.TokenConstants;
import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.request.VerificationCodeDTO;
import com.star.internalcommon.responese.DriverUserExistsResponse;
import com.star.internalcommon.responese.NumberCodeResponse;
import com.star.internalcommon.responese.TokenResponse;
import com.star.internalcommon.util.JwtUtils;
import com.star.internalcommon.util.RedisPrefixUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class VerificationCodeService {

    @Autowired
    private ServiceDriverUserClient serviceDriverUserClient;
    @Autowired
    private ServiceVerificationCodeClient serviceVerificationCodeClient;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    public ResponseResult checkAndsendVerificationCode(String driverPhone){
        //查询该手机号码是否存在
        log.info("第一步 -->"+driverPhone);
        ResponseResult<DriverUserExistsResponse> driverUserExistsResponseResponseResult = serviceDriverUserClient.checkDriver(driverPhone);
        DriverUserExistsResponse data = driverUserExistsResponseResponseResult.getData();
        int ifExists = data.getIfExists();
        if (ifExists == DriverCarConstants.DRIVER_NOT_EXISTS){
            return ResponseResult.fail(
                    CommonStatusEnum.DRIVER_NOT_EXISTS.getCode(),
                    CommonStatusEnum.DRIVER_NOT_EXISTS.getValue());
        }
        log.info("司机存在 -->"+driverPhone);
        //获取验证码
        ResponseResult<NumberCodeResponse> numberCodeResult = serviceVerificationCodeClient.getNumberCode(6);
        NumberCodeResponse numberCodeResponse = numberCodeResult.getData();
        int numberCode = numberCodeResponse.getNumberCode();
        log.info("验证码是："+numberCode);

        // 调用第三方发生验证码,第三方：阿里短信服务，腾讯，华信，容联
        // 存入reids。1：key，2：存入value
        String key = RedisPrefixUtils.generatorKeyPhone(driverPhone, IdentityConstant.DRIVER_IDENTITY);
        stringRedisTemplate.opsForValue().set(key,numberCode+"",2, TimeUnit.MINUTES);

        return ResponseResult.success("");
//        return null;
    }

    /**
     * 校验验证码
     * @param driverPhone  手机号码
     * @param verificationCode 验证码
     * @return 拿到 token令牌
     */
    public ResponseResult checkCode(String driverPhone,String verificationCode){
        //根据手机号key，去Redis去读验证码value
        //生成 key
        String key = RedisPrefixUtils.generatorKeyPhone(driverPhone,IdentityConstant.DRIVER_IDENTITY);
        //根据key生成 value【从Redis中获取】
        String codeRedis = stringRedisTemplate.opsForValue().get(key);
        log.info("Redis中的value值："+codeRedis);

        //校验验证码：如果不存在 或者 和Redis中不相等
        if (StringUtils.isEmpty(codeRedis) || !verificationCode.trim().equals(codeRedis.trim())){
            return ResponseResult.fail(
                    CommonStatusEnum.VERIFICATION_CODE_DRROR.getCode(),   //失败的状态
                    CommonStatusEnum.VERIFICATION_CODE_DRROR.getValue()); //失败的信息

        }

        //颁发令牌
        String accessToken = JwtUtils.generatorToken(driverPhone, IdentityConstant.DRIVER_IDENTITY,TokenConstants.ACCESS_TOKEN_TYPE);
        String refreshToken = JwtUtils.generatorToken(driverPhone, IdentityConstant.PASSENGER_IDENTITY, TokenConstants.REFRESH_TOKEN_TYPE);

        //将token缓存到Redis中
        String accessTokenKey = RedisPrefixUtils.generatorTokenKey(driverPhone,IdentityConstant.DRIVER_IDENTITY, TokenConstants.ACCESS_TOKEN_TYPE);
        //设置 token 过期时间【30天】
        stringRedisTemplate.opsForValue().set(accessTokenKey,accessToken,30,TimeUnit.DAYS);

        String refreshTokenKey = RedisPrefixUtils.generatorTokenKey(driverPhone,IdentityConstant.DRIVER_IDENTITY, TokenConstants.REFRESH_TOKEN_TYPE);
        //设置 token 过期时间【31天】
        stringRedisTemplate.opsForValue().set(refreshTokenKey,refreshToken,31,TimeUnit.DAYS);
        //响应
        TokenResponse tokenResponse = new TokenResponse();
        tokenResponse.setAccessToken(accessToken);
        tokenResponse.setRefreshToken(refreshToken);
        return ResponseResult.success(tokenResponse);
    }
}
