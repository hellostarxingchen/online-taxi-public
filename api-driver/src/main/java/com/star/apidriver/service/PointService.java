package com.star.apidriver.service;

import com.star.apidriver.remote.ServiceDriverUserClient;
import com.star.apidriver.remote.ServiceMapClient;
import com.star.internalcommon.dto.Car;
import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.request.ApiDriverPointRequest;
import com.star.internalcommon.request.PointRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @Auther: Star
 * @Datea: 2022/12/12  -12  -12
 * @Description: com.star.apidriver.service
 * @version: 1.0
 */
@Service
public class PointService {

    @Autowired
    private ServiceDriverUserClient serviceDriverUserClient;
    @Autowired
    private ServiceMapClient serviceMapClient;

    public ResponseResult upload(@RequestBody ApiDriverPointRequest apiDriverPointRequest){
        //获取carId
        Long carId = apiDriverPointRequest.getCarId();

        //通过carId 获取 tid，trid，调用 service-driver-user的接口
        ResponseResult<Car> carById = serviceDriverUserClient.getCarById(carId);
        Car car = carById.getData();
        String tid = car.getTid();
        String trid = car.getTrid();


        //调用地图去上传
        PointRequest pointRequest = new PointRequest();
        pointRequest.setTid(tid);
        pointRequest.setTrid(trid);
        //轨迹也上传
        pointRequest.setPoints(apiDriverPointRequest.getPoints());

        return serviceMapClient.upload(pointRequest);
    }
}
