package com.star.apidriver.controller;

import com.star.apidriver.service.OrderService;
import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.request.OrderRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @Auther: Star
 * @Datea: 2022/12/14  -12  -14
 * @Description: com.star.serviceorder.controller
 * @version: 1.0
 * 创建订单/下单
 */
@Slf4j
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;


    //接乘客
    @PostMapping("/to-pick-up-passenger")
    public ResponseResult changerStatus(@RequestBody OrderRequest orderRequest){
        return orderService.toPickUpPassenger(orderRequest);
    }

    //到达乘客目的地
    @PostMapping("/arrived-departure")
    public ResponseResult arrivedDeparture(@RequestBody OrderRequest orderRequest){
        return orderService.arrivedDeparture(orderRequest);
    }

    //司机接到乘客
    @PostMapping("/pick-up-passenger")
    public ResponseResult pickUpPassenger(@RequestBody OrderRequest orderRequest){
        return orderService.pickUpPassenger(orderRequest);
    }

    //乘客到达目的地，行程终止
    @PostMapping("/passenger-getoff")
    public ResponseResult passengerGetoff(@RequestBody OrderRequest orderRequest){
        return orderService.passengerGetoff(orderRequest);
    }

//    //乘客到达目的地，行程终止
//    @PostMapping("/passenger-getoff")
//    public ResponseResult passengerGetof(@RequestBody OrderRequest orderRequest){
//        return orderService.passengerGetoff(orderRequest);
//    }

}
