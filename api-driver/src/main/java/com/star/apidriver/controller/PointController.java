package com.star.apidriver.controller;

import com.star.apidriver.service.PointService;
import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.request.ApiDriverPointRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/point")
@Slf4j
public class PointController {

    @Autowired
    private PointService pointService;

    @PostMapping("/upload")
    public ResponseResult upload(@RequestBody ApiDriverPointRequest apiDriverPointRequest){
        log.info("这是上传车辆信息！");
        return pointService.upload(apiDriverPointRequest);
    }
}
