package com.star.apidriver.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @GetMapping("auth")
    public String test() {
        return "";
    }

    @GetMapping("noauth")
    public String testNo() {
        return "";
    }

}
