package com.star.apidriver.remote;

import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.request.OrderRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @Auther: Star
 * @Datea: 2023/2/21  -02  -21
 * @Description: com.star.apidriver.remote
 * @version: 1.0
 */
@FeignClient("service-order")
public interface ServiceOrderClient {

    @RequestMapping(method = RequestMethod.POST,value = "/to-pick-up-passenger")
    public ResponseResult changerStatus(@RequestBody OrderRequest orderRequest);

    @RequestMapping(method = RequestMethod.POST,value = "/arrived-departure")
    public ResponseResult arrivedDeparture(@RequestBody OrderRequest orderRequest);

    @RequestMapping(method = RequestMethod.POST,value = "/pick-up-passenger")
    public ResponseResult pickUpPassenger(@RequestBody OrderRequest orderRequest);

    @RequestMapping(method = RequestMethod.POST,value = "/passenger-getoff")
    public ResponseResult passengerGetoff(@RequestBody OrderRequest orderRequest);
}
