package com.star.apidriver.remote;

import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.request.PointRequest;
import com.star.internalcommon.responese.TerminalResponse;
import com.star.internalcommon.responese.TrackResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("service-map")
public interface ServiceMapClient {

    @RequestMapping(method = RequestMethod.POST,value = "/point/upload")
    public ResponseResult upload(@RequestBody PointRequest pointRequest);

//    @RequestMapping(method = RequestMethod.POST,value = "/track/add")
//    public ResponseResult<TrackResponse> addTrack(@RequestParam String tid);

}
