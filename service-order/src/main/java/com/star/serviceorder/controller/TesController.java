package com.star.serviceorder.controller;

import com.star.internalcommon.dto.OrderInfo;
import com.star.serviceorder.mapper.OrderInfoMapper;
import com.star.serviceorder.service.OrderInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * 并发测试：
 * 这里有两个服务，但是端口不一样
 */
@RestController
@Slf4j
public class TesController {

    @Autowired
    private OrderInfoService orderInfoService;
    @Autowired
    private OrderInfoMapper orderInfoMapper;

    @Value("${server.port}")
    String port;

    @GetMapping("/test-order/{orderId}")
    public String dispatchRealTAimeOrder(@PathVariable("orderId") long orderId){
        log.info("这个是service-order-->  并发测试："+orderId+"，端口号："+port);
        OrderInfo orderInfo = orderInfoMapper.selectById(orderId);
        orderInfoService.dispatchRealTimeOrder(orderInfo);
        return "this ok, 2022/12/19";
    }
}
