package com.star.serviceorder.mapper;

import com.star.internalcommon.dto.OrderInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * @author 星辰
 * @since 2022-12-14
 */
@Repository
public interface OrderInfoMapper extends BaseMapper<OrderInfo> {

}
