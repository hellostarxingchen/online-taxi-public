package com.star.serviceorder.service;

import cn.hutool.http.HttpUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.star.internalcommon.constant.AmapConfigConstants;
import com.star.internalcommon.constant.CommonStatusEnum;
import com.star.internalcommon.constant.IdentityConstant;
import com.star.internalcommon.constant.OrderConstants;
import com.star.internalcommon.dto.Car;
import com.star.internalcommon.dto.OrderInfo;
import com.star.internalcommon.dto.PriceRule;
import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.request.OrderRequest;
import com.star.internalcommon.responese.GetTidTridResponese;
import com.star.internalcommon.responese.OrderDriverResponse;
import com.star.internalcommon.responese.TerminalResponse;
import com.star.internalcommon.responese.TrsearchResponse;
import com.star.internalcommon.util.RedisPrefixUtils;
import com.star.serviceorder.mapper.OrderInfoMapper;
import com.star.serviceorder.remote.ServiceDriverUserClient;
import com.star.serviceorder.remote.ServiceMapClient;
import com.star.serviceorder.remote.ServicePriceClient;
import com.star.serviceorder.remote.ServiceSsePushClient;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.http.client.utils.HttpClientUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author 星辰
 * @since 2022-12-14
 */
@Service
@Slf4j
public class OrderInfoService {

    @Value("${amap.key}")
    public String amapKey;
    @Value("${amap.sid}")
    public String amapSid;

    @Autowired
    private OrderInfoMapper orderInfoMapper;
    @Autowired
    private ServicePriceClient servicePriceClient;
    @Autowired
    private ServiceDriverUserClient serviceDriverUserClient;
    @Autowired
    private ServiceMapClient serviceMapClient;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private RedissonClient redissonClient;
    @Autowired
    private ServiceSsePushClient serviceSsePushClient;
    @Autowired(required = false)
    private RestTemplate restTemplate;

    //添加订单
    public ResponseResult add(@RequestBody OrderRequest orderRequest) {

        ResponseResult<Boolean> availableDriver = serviceDriverUserClient.isAvailableDriver(orderRequest.getAddress());
        log.info("测试：当前城市是否有司机？" + availableDriver.getData());
        if (!availableDriver.getData()) {
            return ResponseResult.fail(
                    CommonStatusEnum.CITY_DRIVER_EMPTY.getCode(),
                    CommonStatusEnum.CITY_DRIVER_EMPTY.getValue());
        }

        //需要判断，下单的设备是否为黑名单中的设备
        String deviceCode = orderRequest.getDeviceCode();
        //生成 key
        String deviceCodeKey = RedisPrefixUtils.blackDeviceCodePrefix + deviceCode;
        //判断 key，检查原来有没 key
        if (isBlackDevice(deviceCodeKey)) return ResponseResult.fail(
                CommonStatusEnum.DEVICE_IS_BLACK.getCode(),
                CommonStatusEnum.DEVICE_IS_BLACK.getValue());

        //判断，下单的城市和计价规则是否正常
        if (!isPriceRuleExists(orderRequest)) {
            return ResponseResult.fail(
                    CommonStatusEnum.CITY_SERVICE_NOT_SERVICE.getCode(),
                    CommonStatusEnum.CITY_SERVICE_NOT_SERVICE.getValue());
        }

        //判断，如果【乘客】有订单尚未结束，则不能继续创建订单，订单ID号 PassengerId
        if (isPassengerOrderGoingon(orderRequest.getPassengerId()) > 0) {
            return ResponseResult.fail(
                    CommonStatusEnum.ORDER_GOING_ON.getCode(),
                    CommonStatusEnum.ORDER_GOING_ON.getValue());
        }
        //创建订单
        OrderInfo orderInfo = new OrderInfo();
        BeanUtils.copyProperties(orderRequest, orderInfo);
        orderInfo.setOrderStatus(OrderConstants.ORDER_START);

        LocalDateTime now = LocalDateTime.now();
        orderInfo.setGmtCreate(now);
        orderInfo.setGmtModified(now);

        orderInfoMapper.insert(orderInfo);

        //定时任务
        for (int i = 0; i < 6; i++) {
            //派单逻辑
            int result = dispatchRealTimeOrder(orderInfo);
            if (result == 1) {
                break;
            }
            try {
                Thread.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return ResponseResult.success("");
    }


    /**
     * 实时订单派单逻辑
     *
     * @param orderInfo 订单信息
     * @return 如果返回 1，则派单成功
     * 注意：这个synchronize由于是JVM级别的，所以在多个服务中无法实现负载均衡以及并发问题
     */
    public synchronized int dispatchRealTimeOrder(@RequestBody OrderInfo orderInfo) {
        log.info("循环一次");
        int result = 0;
        //定位坐标【经纬度：维度在前，经度在后】
        String depLatitude = orderInfo.getDepLatitude();
        String depLongitude = orderInfo.getDepLongitude();
        String center = depLatitude + "," + depLongitude;

        List<Integer> radiusList = new ArrayList<>();
        radiusList.add(2000);
        radiusList.add(4000);
        radiusList.add(5000);
        //搜索结果
        ResponseResult<List<TerminalResponse>> listResponseResult = null;
        radius:
        for (int i = 0; i < radiusList.size(); i++) {
            Integer radius = radiusList.get(i);
            listResponseResult = serviceMapClient.terminalAroundSearch(center, radius);
            log.info("在此半径为：" + radius + "范围内找到车辆：" + JSONArray.fromObject(listResponseResult.getData()).toString());
            //获得终端

            //解析终端
            List<TerminalResponse> data = listResponseResult.getData();
            //测试代码【找不到车情况】
//            List<TerminalResponse> data = new ArrayList<>();
            for (int j = 0; j < data.size(); j++) {
                TerminalResponse terminalResponse = data.get(j);
                Long carId = terminalResponse.getCarId();

                String longitude = terminalResponse.getLongitude();
                String latitude = terminalResponse.getLatitude();

                //查询是否有可派单的司机
                ResponseResult<OrderDriverResponse> availableDriver = serviceDriverUserClient.getAvailableDriver(carId);
                if (availableDriver.getCode() == CommonStatusEnum.AVAILABLE_DRIVER_EMPTY.getCode()) {
                    log.info("没有对应的车辆的ID：" + carId + "，对应的司机");
                    continue radius;
                } else {
                    log.info("车辆ID：" + carId + "找到了正在出车的司机");
                    OrderDriverResponse orderDriverResponse = availableDriver.getData();
                    Long driverId = orderDriverResponse.getDriverId();
                    String driverPhone = orderDriverResponse.getDriverPhone();
                    String licenseId = orderDriverResponse.getLicenseId();
                    String vehicleNo = orderDriverResponse.getVehicleNo();
                    String vehicleTypeFromCar = orderDriverResponse.getVehicleType();

                    //判断车辆是否符合车型 ？
                    String vehicleType = orderInfo.getVehicleType();
                    if (vehicleType.trim().equals(vehicleTypeFromCar.trim())){
                        log.info("车辆不符合");
                        continue ;
                    }

                    //使用Redis锁，解决并发问题
                    String lockKey = (driverId + "").intern();
                    RLock lock = redissonClient.getLock(lockKey);
                    lock.lock(); //上锁

                    //锁的细粒度化【解决并发问题】
//                    synchronized ((driverId + "").intern()) {
                    //判断，如果【乘客】有订单尚未结束，则不能继续创建订单，订单ID号 PassengerId
                    if (isDriverOrderGoingon(driverId) > 0) {
                        lock.unlock();
                        continue;
                    }

                    //订单直接匹配司机，并且查询当前车辆信息
                    QueryWrapper<Car> queryWrapper = new QueryWrapper<>();
                    queryWrapper.eq("id", carId);


                    //设置订单中的司机车辆相关信息
                    orderInfo.setDriverId(driverId);
                    orderInfo.setDriverPhone(driverPhone);
                    orderInfo.setCarId(carId);
                    //经纬度信息从地图中获取
                    orderInfo.setReceiveOrderCarLongitude(longitude);
                    orderInfo.setReceiveOrderCarLatitude(latitude);

                    orderInfo.setReceiveOrderTime(LocalDateTime.now());
                    orderInfo.setLicenseId(licenseId);
                    orderInfo.setVehicleNo(vehicleNo);
                    orderInfo.setOrderStatus(OrderConstants.DRIVER_REECIVE_ORDER);

                    orderInfoMapper.updateById(orderInfo);

                    //通知司机【乘客的信息】
                    JSONObject driverContent = new JSONObject();
                    driverContent.put("passengerId", orderInfo.getPassengerId());
                    driverContent.put("passenger", orderInfo.getPassengerPhone());
                    driverContent.put("departure", orderInfo.getDeparture());
                    driverContent.put("depLongitude", orderInfo.getDepLongitude());
                    driverContent.put("depLatitude", orderInfo.getDepLatitude());

                    //通知司机【乘客的位置】
                    driverContent.put("destination", orderInfo.getDestination());
                    driverContent.put("destLongitude", orderInfo.getDestLongitude());
                    driverContent.put("destLatitude", orderInfo.getDestLatitude());
                    //把乘客的信息推送到司机端
                    serviceSsePushClient.push(driverId, IdentityConstant.DRIVER_IDENTITY, driverContent.toString());

                    //通知乘客【司机信息】
                    JSONObject passengerContent = new JSONObject();
                    passengerContent.put("driverId", orderInfo.getDriverId());
                    passengerContent.put("driverPhone", orderInfo.getDriverPhone());
                    passengerContent.put("vehicleNo", orderInfo.getVehicleNo());
                    //通知乘客【车辆信息】
                    ResponseResult<Car> carById = serviceDriverUserClient.getCarById(orderInfo.getCarId());
                    Car carRemote = carById.getData();
                    passengerContent.put("brand", carRemote.getBrand());
                    passengerContent.put("model", carRemote.getModel());
                    passengerContent.put("vehicleColor", carRemote.getVehicleColor());

                    //通知乘客【位置信息】
                    passengerContent.put("receiveOrderCarLongitude", orderInfo.getReceiveOrderCarLongitude());
                    passengerContent.put("receiveOrderCarLatitude", orderInfo.getReceiveOrderCarLatitude());
//                    passengerContent.put("destLatitude",orderInfo.get());
                    //把司机的信息推送到乘客端
                    serviceSsePushClient.push(orderInfo.getPassengerId(), IdentityConstant.PASSENGER_IDENTITY, passengerContent.toString());
                    result = 1;
                    //执行完毕，释放锁
                    lock.unlock();

                    //退出，不在进行 司机的查找
                    break radius;

//                    }
                }
            }

            //根据解析出来的终端，查询车辆

            //找到符合的车辆，进行派单
        }
        return result;

    }

    /**
     * 计价规则是否存在
     *
     * @param orderRequest 订单的请求
     * @return 返回结果 true 或者 false
     */
    private boolean isPriceRuleExists(OrderRequest orderRequest) {
        String fareType = orderRequest.getFareType();
        int index = fareType.indexOf("$");
        String cityCode = fareType.substring(0, index);
        String vehicleTytpe = fareType.substring(index + 1);

        PriceRule priceRule = new PriceRule();
        priceRule.setCityCode(cityCode);
        priceRule.setVehicleType(vehicleTytpe);

        ResponseResult<Boolean> responseResult = servicePriceClient.ifPriceExists(priceRule);
        return responseResult.getData();
    }

    /**
     * 是否是黑名单
     *
     * @param deviceCodeKey 设备
     * @return 结果只有两个
     */
    private boolean isBlackDevice(String deviceCodeKey) {
        Boolean aBoolean = stringRedisTemplate.hasKey(deviceCodeKey);

        if (aBoolean) {
            String s = stringRedisTemplate.opsForValue().get(deviceCodeKey);
            int i = Integer.parseInt(s);
            if (i < 2) {
                //说明该设备超过下单两次了，就不能继续下单了
                return true;
            } else {
                stringRedisTemplate.opsForValue().increment(deviceCodeKey);
            }
        } else {
            //同一个设备一个小时内只能下单一次
            stringRedisTemplate.opsForValue().setIfAbsent(deviceCodeKey, "1", 1L, TimeUnit.HOURS);
        }
        return false;
    }

    /**
     * 判断是否有【乘客的】订单在进行
     *
     * @param passengerId 乘客id
     * @return 是否有 ？
     */
    private Integer isPassengerOrderGoingon(Long passengerId) {

        //判断是否有正在进行的订单【不准下单】
        QueryWrapper<OrderInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("passenger_id", passengerId);
        queryWrapper.and(wrapper -> wrapper.eq("order_status", OrderConstants.ORDER_START)
                .or().eq("order_status", OrderConstants.DRIVER_REECIVE_ORDER)
                .or().eq("order_status", OrderConstants.DRIVER_TO_PICK_UP_PASSENGER)
                .or().eq("order_status", OrderConstants.DRIVER_ARRIVED_DEPARTURE)
                .or().eq("order_status", OrderConstants.PICK_UP_PASSENGER)
                .or().eq("order_status", OrderConstants.PASSENGER_GETOFF)
                .or().eq("order_status", OrderConstants.TO_START_PAY));

        Integer validOrderNumber = orderInfoMapper.selectCount(queryWrapper);

        return validOrderNumber;
    }

    /**
     * 判断是否有【司机的】订单在进行
     *
     * @param driverId 司机id
     * @return 结果
     */
    private Integer isDriverOrderGoingon(Long driverId) {

        //判断是否有正在进行的订单【不准下单】
        QueryWrapper<OrderInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("driver_id", driverId);
        queryWrapper.and(wrapper -> wrapper
                .eq("order_status", OrderConstants.DRIVER_REECIVE_ORDER)
                .or().eq("order_status", OrderConstants.DRIVER_TO_PICK_UP_PASSENGER)
                .or().eq("order_status", OrderConstants.DRIVER_ARRIVED_DEPARTURE)
                .or().eq("order_status", OrderConstants.PICK_UP_PASSENGER));

        Integer validOrderNumber = orderInfoMapper.selectCount(queryWrapper);
        log.info("司机的Id" + driverId + "，有正在进行的订单数量：" + validOrderNumber);
        return validOrderNumber;
    }

    /**
     * 去接乘客
     *
     * @param orderRequest 参数是订单请求信息
     * @return 返回结果级
     */
    public ResponseResult toPickUpPassenger(OrderRequest orderRequest) {
        Long orderId = orderRequest.getOrderId();
        LocalDateTime toPickUpPassengerTime = orderRequest.getToPickUpPassengerTime();
        String toPickUpPassengerLongitude = orderRequest.getToPickUpPassengerLongitude();
        String toPickUpPassengerLatitude = orderRequest.getToPickUpPassengerLatitude();
        String toPickUpPassengerAddress = orderRequest.getToPickUpPassengerAddress();
        QueryWrapper<OrderInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("id", orderId);
        OrderInfo orderInfo = orderInfoMapper.selectOne(queryWrapper);

        orderInfo.setToPickUpPassengerAddress(toPickUpPassengerAddress);
        orderInfo.setToPickUpPassengerLatitude(toPickUpPassengerLatitude);
        orderInfo.setToPickUpPassengerLongitude(toPickUpPassengerLongitude);
        orderInfo.setReceiveOrderTime(LocalDateTime.now());
        orderInfo.setOrderStatus(OrderConstants.DRIVER_TO_PICK_UP_PASSENGER);

        orderInfoMapper.updateById(orderInfo);
        return ResponseResult.success();
    }

    /**
     * 司机到达乘客的目的地
     *
     * @param orderRequest 订单信息
     * @return 返回结果
     */
    public ResponseResult arrivedDeparture(@RequestBody OrderRequest orderRequest) {
        Long orderId = orderRequest.getOrderId();
        QueryWrapper<OrderInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("id", orderId);
        OrderInfo orderInfo = orderInfoMapper.selectOne(queryWrapper);
        orderInfo.setOrderStatus(OrderConstants.DRIVER_ARRIVED_DEPARTURE);

        orderInfo.setDriverArrivedDepartureTime(LocalDateTime.now());
        orderInfoMapper.updateById(orderInfo);

        return ResponseResult.success();
    }

    /**
     * 司机接到乘客
     *
     * @param orderRequest 订单信息
     * @return 返回结果
     */
    public ResponseResult pickUpPassenger(@RequestBody OrderRequest orderRequest) {
        Long orderId = orderRequest.getOrderId();
        QueryWrapper<OrderInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("id", orderId);
        OrderInfo orderInfo = orderInfoMapper.selectOne(queryWrapper);

        orderInfo.setPickUpPassengerLongitude(orderRequest.getPickUpPassengerLongitude());
        orderInfo.setPickUpPassengerLatitude(orderRequest.getPickUpPassengerLatitude());
        orderInfo.setPickUpPassengerTime(LocalDateTime.now());
        orderInfo.setOrderStatus(OrderConstants.PICK_UP_PASSENGER);

        orderInfoMapper.updateById(orderInfo);
        return ResponseResult.success();
    }

    /**
     * 乘客下车，行程终止
     *
     * @param orderRequest 订单信息
     * @return 返回结果
     */
    public ResponseResult passengerGetoff(@RequestBody OrderRequest orderRequest) {
        Long orderId = orderRequest.getOrderId();
        QueryWrapper<OrderInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("id", orderId);
        OrderInfo orderInfo = orderInfoMapper.selectOne(queryWrapper);

        orderInfo.setPassengerGetoffTime(LocalDateTime.now());
        orderInfo.setPassengerGetoffLongitude(orderRequest.getPassengerGetoffLongitude());
        orderInfo.setPassengerGetoffLatitude(orderRequest.getPassengerGetoffLatitude());
        orderInfo.setOrderStatus(OrderConstants.PASSENGER_GETOFF);

        //订单行驶的路程和时间,调用 service-map
        ResponseResult<Car> carById = serviceDriverUserClient.getCarById(orderInfo.getCarId());
        Long starttime = orderInfo.getPickUpPassengerTime().toInstant(ZoneOffset.of("+8")).toEpochMilli();
        LocalDateTime.now().toInstant(ZoneOffset.of("+8"));
        Long endtime = LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli();
        log.info("开始时间：" + starttime);
        log.info("结束时间：" + endtime);

        //调用司机用户服务接口获取到车辆的 itd 和 trid
        ResponseResult<GetTidTridResponese> carTidById = serviceDriverUserClient.getCarTidById(orderInfo.getCarId());
        GetTidTridResponese carTidByIdData = carTidById.getData();
        String tid = carTidByIdData.getTid();
        String trid = carTidByIdData.getTrid();

        //接下来就是上传轨迹信息
        OrderInfo orderInfoid = orderInfoMapper.selectById(orderInfo.getId());
        String pickUpPassengerLongitude = orderInfoid.getPickUpPassengerLongitude();
        String pickUpPassengerLatitude = orderInfoid.getPickUpPassengerLatitude();

        //上传轨迹【获取起点】的请求参数 tid trid 时间，经纬度
        //从数据库中获取的参数做拼接
        String positionlibrary = pickUpPassengerLongitude + "," + pickUpPassengerLatitude;


        //从前端传递进来的参数做拼接
        String passengerGetoffLongitude = orderRequest.getPassengerGetoffLongitude();
        String passengerGetoffLatitude = orderRequest.getPassengerGetoffLatitude();
        String positionparameter = passengerGetoffLongitude + "," + passengerGetoffLatitude;

        //上传轨迹信息【关键的信息】
        JSONObject body = new JSONObject();
        body.put("key", amapKey);
        body.put("sid", amapSid);
        body.put("tid", tid);
        body.put("trid", trid);

        //设置  经纬度，时间（以数组的形式）
        JSONArray points = new JSONArray();
        JSONObject point1 = new JSONObject();
        point1.put("location", positionlibrary);
        point1.put("locatetime", starttime);
        points.add(point1);

        JSONObject point2 = new JSONObject();
        point2.put("location", positionparameter);
        point2.put("locatetime", endtime);
        points.add(point2);
        body.put("points", points);

        String bodyStr = body.toString();
        log.info("请求为：==> "+body);
        String response = HttpUtil.post(AmapConfigConstants.POINT_UPLOAD, bodyStr);
        JSONObject responseJson = JSONObject.fromObject(response);
//        String location = responseJson.getString("location");
//        log.info("位置信息："+location);

        // 查询轨迹计算价格
        ResponseResult<TrsearchResponse> trsearch = serviceMapClient.trsearch(carById.getData().getTid(), starttime, endtime);
        TrsearchResponse data = trsearch.getData();

        orderInfo.setDriveMile(data.getDriverMile());
        orderInfo.setDriveTime(data.getDriverTime());

        orderInfoMapper.updateById(orderInfo);
        return ResponseResult.success();
    }

}
