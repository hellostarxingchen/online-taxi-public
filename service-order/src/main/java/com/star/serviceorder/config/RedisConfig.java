package com.star.serviceorder.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * @Auther: Star
 * @Datea: 2023/1/26  -01  -26
 * @Description: com.star.serviceorder.config
 * @version: 1.0
 */
@Component
public class RedisConfig {

    private String potocol ="redis://";

    @Value("spring.redis.host")
    private String redisHost;

    @Value("spring.redis.port")
    private String redisPort;

    @Bean
    public RedissonClient redissonClient(){
        Config config = new Config();
        config.useSingleServer().setAddress(potocol+redisHost+":"+redisPort).setDatabase(0);

        return Redisson.create();

    }
}
