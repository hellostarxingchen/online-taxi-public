package com.star.serviceorder.remote;

import com.star.internalcommon.dto.PriceRule;
import com.star.internalcommon.dto.ResponseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;


/**
 * @Auther: Star
 * @Datea: 2022/12/16  -12  -16
 * @Description: com.star.serviceorder.remote
 * @version: 1.0
 */
@FeignClient("service-price")
public interface ServicePriceClient {

    @GetMapping( "/price-rule/if-exists")
    public ResponseResult<Boolean> ifPriceExists(@RequestBody PriceRule priceRule);
}
