package com.star.serviceorder.remote;

import com.star.internalcommon.dto.Car;
import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.responese.GetTidTridResponese;
import com.star.internalcommon.responese.OrderDriverResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Auther: Star
 * @Datea: 2022/12/17  -12  -17
 * @Description: com.star.serviceorder.remote
 * @version: 1.0
 */
@FeignClient("service-driver-user")
public interface ServiceDriverUserClient {

    @GetMapping("/city-driver/is-alailable-driver")
    public ResponseResult<Boolean> isAvailableDriver(@RequestParam String cityCode);

    @GetMapping("/get-available-driver/{carId}")
    public ResponseResult<OrderDriverResponse> getAvailableDriver(@PathVariable("carId") Long carId);

    @GetMapping("/car")
    public ResponseResult<Car> getCarById(@RequestParam  Long carId);

    @PostMapping("/cartid")
    public ResponseResult<GetTidTridResponese> getCarTidById(@RequestParam  Long carId);

}
