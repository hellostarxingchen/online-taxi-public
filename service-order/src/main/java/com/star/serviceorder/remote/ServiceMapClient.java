package com.star.serviceorder.remote;

import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.request.PointRequest;
import com.star.internalcommon.responese.TerminalResponse;
import com.star.internalcommon.responese.TrsearchResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Auther: Star
 * @Datea: 2022/12/19  -12  -19
 * @Description: com.star.serviceorder.remote
 * @version: 1.0 调用地图服务
 */
@FeignClient("service-map")
public interface ServiceMapClient {

    //终端搜索
    @RequestMapping(method = RequestMethod.POST,value = "/terminal/aroundsearch")
    public ResponseResult<List<TerminalResponse>> terminalAroundSearch(
            @RequestParam String center,
            @RequestParam Integer radius);

    //搜索轨迹信息
    @RequestMapping(method = RequestMethod.POST,value = "/terminal/trsearch")
    public ResponseResult<TrsearchResponse> trsearch(
            @RequestParam String tid,
            @RequestParam Long starttime,
            @RequestParam Long endtime);

}
