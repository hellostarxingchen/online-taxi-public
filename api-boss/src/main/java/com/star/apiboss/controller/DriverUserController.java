package com.star.apiboss.controller;

import com.star.apiboss.service.CarService;
import com.star.apiboss.service.DriverUserService;
import com.star.internalcommon.dto.Car;
import com.star.internalcommon.dto.DriverUser;
import com.star.internalcommon.dto.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DriverUserController {

    @Autowired
    private CarService carService;

    @Autowired
    private DriverUserService driverUserService;

    //添加司机
    @PostMapping("/driver-user")
    public ResponseResult addDriver(@RequestBody DriverUser driverUser) {
        return driverUserService.addDriverUser(driverUser);
    }

    //修改司机
    @PutMapping("/driver-user")
    public ResponseResult updateDriver(@RequestBody DriverUser driverUser) {
        return driverUserService.updateDriverUser(driverUser);
    }

    @PostMapping("/car")
    public ResponseResult car(@RequestBody Car car) {
        return carService.addCar(car);
    }

}
