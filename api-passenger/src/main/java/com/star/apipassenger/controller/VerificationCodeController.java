package com.star.apipassenger.controller;

import com.star.apipassenger.service.VerificationCodeService;
import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.request.VerificationCodeDTO;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 服务的调用者，提供手机号码获取验证码
 */
@RestController
public class VerificationCodeController {

    @Autowired
    private VerificationCodeService verificationCodeService;

    //获取手机号码
    @GetMapping("/verification-code")
    public ResponseResult verificationCode(
            @RequestBody VerificationCodeDTO verificationCodeDTO) throws JSONException {
        String phone = verificationCodeDTO.getPassengerPhone();
        return verificationCodeService.generatorCode(phone);
    }

    /**
     * 验证手机号码和验证码
     * @param verificationCodeDTO 接口中的参数
     * @return 返回校验结果
     */
    @PostMapping("/verification-code-check")
    public ResponseResult checkVerificationCode(
            @RequestBody VerificationCodeDTO verificationCodeDTO){

        String passengerPhone = verificationCodeDTO.getPassengerPhone();
        String verificationCode = verificationCodeDTO.getVerificationCode();
        System.out.println("手机号码："+passengerPhone+"，验证码："+verificationCode);

        return verificationCodeService.checkCode(passengerPhone,verificationCode);
    }
}
