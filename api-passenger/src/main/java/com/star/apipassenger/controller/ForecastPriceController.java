package com.star.apipassenger.controller;

import com.star.apipassenger.remote.ServicePriceClient;
import com.star.apipassenger.service.ForecastPriceService;
import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.request.ForecastPriceDTO;
import com.star.internalcommon.responese.ForecastPriceResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class ForecastPriceController {

    @Autowired
    private ForecastPriceService forecastPriceService;

    @PostMapping("/forecast-price")
    public ResponseResult forecastPrice(@RequestBody ForecastPriceDTO forecastPriceDTO){

        //调用计价服务
        String depLongitude = forecastPriceDTO.getDepLongitude();
        String depLatitude = forecastPriceDTO.getDepLatitude();
        String destLongitude = forecastPriceDTO.getDestLongitude();
        String destLatitude = forecastPriceDTO.getDestLatitude();
        String cityCode = forecastPriceDTO.getCityCode();
        String vehicleType = forecastPriceDTO.getVehicleType();

        log.info("出发地的经纬度："+depLongitude+"，"+depLatitude);
        log.info("目的地的经纬度："+destLongitude+"，"+destLatitude);

        return forecastPriceService.forecastPrice(
                depLongitude,
                depLatitude,
                destLongitude,
                destLatitude,
                cityCode,
                vehicleType);
    }
}
