package com.star.apipassenger.controller;

import com.star.apipassenger.service.TokenService;
import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.responese.TokenResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TokenController {

    @Autowired
    private TokenService tokenService;

    @PostMapping("/token-refresh")
    public ResponseResult refreshToken(@RequestBody TokenResponse tokenResponse){

        String refreshTokenStr = tokenResponse.getRefreshToken();
        System.out.println(refreshTokenStr);
        return tokenService.refreshToken(refreshTokenStr);
    }
}
