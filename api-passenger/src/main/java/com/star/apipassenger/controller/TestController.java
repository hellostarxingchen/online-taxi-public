package com.star.apipassenger.controller;

import com.star.apipassenger.remote.ServiceOrderClient;
import com.star.internalcommon.dto.OrderInfo;
import com.star.internalcommon.dto.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Autowired
    private ServiceOrderClient serviceOrderClient;

    //这个是有token的方法
    @GetMapping("/authTest")
    public ResponseResult test() {
        return ResponseResult.success("auth !");
    }

    //这个是没有token的方法
    @GetMapping("/noauthTest")
    public ResponseResult test1() {
        return ResponseResult.success("noauth tt");
    }

    @GetMapping("/test-order/{orderId}")
    public String dispatchRealTAimeOrder(@PathVariable("orderId") long orderId){
        System.out.println("这个是 api-passenger--> 并发测试："+orderId);
        serviceOrderClient.dispatchRealTAimeOrder(orderId);
        return "this ok, 2022/12/28";
    }
}
