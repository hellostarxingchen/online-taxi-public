package com.star.apipassenger.controller;

import com.star.apipassenger.service.OrderService;
import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.request.OrderRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Auther: Star
 * @Datea: 2022/12/14  -12  -14
 * @Description: com.star.apidriver.controller
 * @version: 1.0
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    /**
     * 创建订单/下单
     */

    @Autowired
    private OrderService orderService;

    @PostMapping("/add")
    public ResponseResult add(@RequestBody OrderRequest orderRequest){
//        System.out.println(orderRequest);
        return orderService.add(orderRequest);

    }

}
