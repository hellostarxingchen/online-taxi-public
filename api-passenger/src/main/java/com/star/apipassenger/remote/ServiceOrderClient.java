package com.star.apipassenger.remote;

import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.request.OrderRequest;
import com.star.internalcommon.request.VerificationCodeDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * @Auther: Star
 * @Datea: 2022/12/14  -12  -14
 * @Description: com.star.apipassenger.remote
 * @version: 1.0
 */
@FeignClient("service-order")
public interface ServiceOrderClient {

    @RequestMapping(method = RequestMethod.POST,value = "/order/add")
    public ResponseResult add(@RequestBody OrderRequest orderRequest);

    @RequestMapping(method = RequestMethod.GET,value ="/test-order/{orderId}")
    public String dispatchRealTAimeOrder(@PathVariable("orderId") long orderId);
}
