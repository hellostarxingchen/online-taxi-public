package com.star.apipassenger.remote;

import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.responese.NumberCodeResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 调用验证码服务，服务名和 nacos中的一致
 * 【注意】该接口不能有 /
 */

@FeignClient("service-verificationcode")
public interface ServiceVefificationcodeClient {

    //传递验证码的位数，用于校验
    @RequestMapping(method = RequestMethod.GET,value = "/numberCode/{size}")
    ResponseResult<NumberCodeResponse> getNumberCode(@PathVariable("size")int size);

}
