package com.star.apipassenger.remote;

import com.star.internalcommon.dto.PassengerUser;
import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.request.VerificationCodeDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 调用服务，服务名字要和 nacos上的一致
 * 【注意】该接口不能有 /
 */
@FeignClient("service-passenger-user")
public interface ServicePassengerUserClient {

    @RequestMapping(method = RequestMethod.POST,value = "/user")
    public ResponseResult loginOrRegister(@RequestBody VerificationCodeDTO verificationCodeDTO);

    @RequestMapping(method = RequestMethod.GET,value = "/user/")
    public ResponseResult<PassengerUser> getUserByPhone(@RequestBody VerificationCodeDTO verificationCodeDTO);



}
