package com.star.apipassenger.service;

import com.star.apipassenger.remote.ServiceOrderClient;
import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.request.OrderRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Auther: Star
 * @Datea: 2022/12/14  -12  -14
 * @Description: com.star.apipassenger.service
 * @version: 1.0
 */
@Service
public class OrderService {

    @Autowired
    private ServiceOrderClient serviceOrderClient;

    public ResponseResult add(@RequestBody OrderRequest orderRequest){
        return serviceOrderClient.add(orderRequest);
    }
}
