package com.star.apipassenger.service;

import com.alibaba.cloud.commons.lang.StringUtils;
import com.star.apipassenger.remote.ServicePassengerUserClient;
import com.star.apipassenger.remote.ServiceVefificationcodeClient;
import com.star.internalcommon.constant.CommonStatusEnum;
import com.star.internalcommon.constant.IdentityConstant;
import com.star.internalcommon.constant.TokenConstants;
import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.responese.NumberCodeResponse;
import com.star.internalcommon.responese.TokenResponse;
import com.star.internalcommon.request.VerificationCodeDTO;
import com.star.internalcommon.util.JwtUtils;
import com.star.internalcommon.util.RedisPrefixUtils;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * 接收手机号码，发送验证码的业务逻辑
 */
@Service
public class VerificationCodeService {

    /**
     * 将 remote 包下的远程调用服务注入到 service层中
     */

    @Autowired
    private ServiceVefificationcodeClient serviceVefificationcodeClient;

    //因为验证id是String 类型的，所以不用 RedisTemplate
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private ServicePassengerUserClient servicePassengerUserClient;

    /**
     * 在这里拿到手机号码之后就会生成验证码
     * @param passengerPhone 手机号码【要和接口中传递的参数一致】
     * @return 返回成功信息
     * @throws JSONException
     */
    public ResponseResult generatorCode(String passengerPhone) throws JSONException {
        ResponseResult<NumberCodeResponse> numberCodeResponse = serviceVefificationcodeClient.getNumberCode(6);
        int numberCode = numberCodeResponse.getData().getNumberCode();
        System.out.println("remote number code->"+numberCode);

        //存入Redis
        System.out.println("存入Redis");
        //key value
        String key = RedisPrefixUtils.generatorKeyPhone(passengerPhone,IdentityConstant.PASSENGER_IDENTITY);
        //过期时间 【2分钟】
        stringRedisTemplate.opsForValue().set(key,numberCode+"",2, TimeUnit.MINUTES);

        //通过短信服务器，将对应的验证码发送到手机上

        return ResponseResult.success();
    }

    /**
     * 校验验证码
     * @param passengerPhone  手机号码
     * @param verificationCode 验证码
     * @return 拿到 token令牌
     */
    public ResponseResult checkCode(String passengerPhone,String verificationCode){
        //根据手机号key，去Redis去读验证码value
        //生成 key
        String key = RedisPrefixUtils.generatorKeyPhone(passengerPhone,IdentityConstant.PASSENGER_IDENTITY);
        //根据key生成 value【从Redis中获取】
        String codeRedis = stringRedisTemplate.opsForValue().get(key);
        System.out.println("Redis中的value值："+codeRedis);

        //校验验证码：如果不存在 或者 和Redis中不相等
        if (StringUtils.isEmpty(codeRedis) || !verificationCode.trim().equals(codeRedis.trim())){
            return ResponseResult.fail(
                    CommonStatusEnum.VERIFICATION_CODE_DRROR.getCode(),   //失败的状态
                    CommonStatusEnum.VERIFICATION_CODE_DRROR.getValue()); //失败的信息

        }
        //判断是否有用户，并且进行相应的处理
        VerificationCodeDTO verificationCodeDTO = new VerificationCodeDTO();
        verificationCodeDTO.setPassengerPhone(passengerPhone);
        servicePassengerUserClient.loginOrRegister(verificationCodeDTO);

        //颁发令牌
        String accessToken = JwtUtils.generatorToken(passengerPhone, IdentityConstant.PASSENGER_IDENTITY, passengerPhone);
        String refreshToken = JwtUtils.generatorToken(passengerPhone, IdentityConstant.PASSENGER_IDENTITY, passengerPhone);

        //将token缓存到Redis中
        String accessTokenKey = RedisPrefixUtils.generatorTokenKey(passengerPhone,IdentityConstant.PASSENGER_IDENTITY, TokenConstants.ACCESS_TOKEN_TYPE);
        //设置 token 过期时间【30天】
        stringRedisTemplate.opsForValue().set(accessTokenKey,accessToken,30,TimeUnit.DAYS);

        String refreshTokenKey = RedisPrefixUtils.generatorTokenKey(passengerPhone,IdentityConstant.PASSENGER_IDENTITY, TokenConstants.REFRESH_TOKEN_TYPE);
        //设置 token 过期时间【31天】
        stringRedisTemplate.opsForValue().set(refreshTokenKey,refreshToken,31,TimeUnit.DAYS);
        //响应
        TokenResponse tokenResponse = new TokenResponse();
        tokenResponse.setAccessToken(accessToken);
        tokenResponse.setRefreshToken(refreshToken);
        return ResponseResult.success(tokenResponse);
    }


}
