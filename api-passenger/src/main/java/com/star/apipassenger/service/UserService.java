package com.star.apipassenger.service;

import com.star.apipassenger.remote.ServicePassengerUserClient;
import com.star.internalcommon.dto.PassengerUser;
import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.dto.TokenResult;
import com.star.internalcommon.request.VerificationCodeDTO;
import com.star.internalcommon.util.JwtUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserService {

    @Autowired
    private ServicePassengerUserClient servicePassengerUserClient;

    public ResponseResult getUserByAccessToken(String accessToken) {
        //解析 accessToken 拿到手机号
        log.info("accessToken->>>>："+accessToken);
        TokenResult tokenResult = JwtUtils.checkToken(accessToken);
        String phone = tokenResult.getPhone();
        log.info("手机号："+phone);

        //根据手机号查询用户信息
        VerificationCodeDTO verificationCodeDTO = new VerificationCodeDTO();
        verificationCodeDTO.setPassengerPhone(phone);
        ResponseResult<PassengerUser> userByPhone = servicePassengerUserClient.getUserByPhone(verificationCodeDTO);


        return ResponseResult.success(userByPhone.getData());
    }
}
