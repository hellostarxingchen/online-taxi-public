package com.star.apipassenger.interceptor;

import com.alibaba.cloud.commons.lang.StringUtils;
import com.star.internalcommon.constant.TokenConstants;
import com.star.internalcommon.dto.ResponseResult;
import com.star.internalcommon.dto.TokenResult;
import com.star.internalcommon.util.JwtUtils;
import com.star.internalcommon.util.RedisPrefixUtils;
import org.kordamp.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

public class JwtInterceptor implements HandlerInterceptor {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        boolean result = true;
        String resultString = "";
        String token = request.getHeader("Authorization");
        //解析并且校验token
        TokenResult tokenResult = JwtUtils.checkToken(token);

        //判断 token 是否为空
        if (tokenResult == null){
            resultString = "token invalid";
            System.out.println(resultString+"token this null? Please check!!!");
            result = false;
        }else {
            //拼接 key
            String phone = tokenResult.getPhone();
            String identity = tokenResult.getIdentity();

            String tokenKey = RedisPrefixUtils.generatorTokenKey(phone,identity, TokenConstants.ACCESS_TOKEN_TYPE);
            //从Redis中拿到 token
            String tokenRedis = stringRedisTemplate.opsForValue().get(tokenKey);

            //判断Redis中是否为空或者 token是否相等
            if ((StringUtils.isBlank(tokenRedis)) || (!token.trim().equals(tokenRedis.trim()))){
                resultString = "token valid";
                result = false;
            }

            //如果 result 是 false 则
            if (!result){
                PrintWriter out = response.getWriter();
                System.out.println("不好，返回结果为 false");
                out.print(JSONObject.fromObject(ResponseResult.fail(resultString)));
            }

        }

        return true;
    }
}
